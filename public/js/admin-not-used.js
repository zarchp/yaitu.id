$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.delete').click(function(e) {
        var d = confirm('Are you sure want to delete this data?');
        if (d != true)
            e.preventDefault();
    });

    $('.order-id').click(function(e) {
        console.log($(this).html());
        var orderId = $(this).find("strong").text();
        window.location.href = "/admin/order/edit/"+orderId;
    });

    /*$('.country').change(function(){
        var id = $(this).val(),
            options = "<option value=''>-- Select Province --</option>";
        if(id == 'ID' ){
            $('.province').removeAttr('disabled');
        } else {
            $('.province').val('');
            $('.province').attr('disabled', 'disabled');
            $('.regency').val('');
            $('.regency').attr('disabled', 'disabled');
            $('.subdistrict').val('');
            $('.subdistrict').attr('disabled', 'disabled');
        } 
    });*/
    
    $('.province').change(function(){
        var id = $(this).val(),
            options = "<option value=''>-- Select Regency --</option>",
            options2 = "<option value=''>-- Select Subdistrict --</option>",
            options3 = "<option value=''>-- Select Village --</option>";
        $.get( "/admin/ajax/regency/"+id, function( result ) {
            $.each(JSON.parse(result), function(index, data){
                options += "<option value='"+data.kabupaten_id+"'>"+data.kabupaten_name+"</option>"
            });
            $('.regency').html(options);
            $('.subdistrict').html(options2);
            $('.village').html(options3);
            if(id){
                $('.regency').removeAttr('disabled');
            } else {
                $('.regency').attr('disabled', 'disabled');
            }
            $('.subdistrict').val('');
            $('.subdistrict').attr('disabled', 'disabled');
            $('.village').val('');
            $('.village').attr('disabled', 'disabled');
        });    
    });

    $('.regency').change(function(){
        var id = $(this).val(),
            options = "<option value=''>-- Select Subdistrict --</option>";
        $.get( "/admin/ajax/subdistrict/"+id, function( result ) {
            $.each(JSON.parse(result), function(index, data){
                options += "<option value='"+data.kecamatan_id+"'>"+data.kecamatan_name+"</option>"
            });
            $('.subdistrict').html(options);
            if(id){
                $('.subdistrict').removeAttr('disabled');
            } else {
                $('.subdistrict').attr('disabled', 'disabled');
            }
            $('.village').val('');
            $('.village').attr('disabled', 'disabled');
        });    
    });

    $('.subdistrict').change(function(){
        var id = $(this).val(),
            options = "<option value=''>-- Select Village --</option>";
        $.get( "/admin/ajax/village/"+id, function( result ) {
            $.each(JSON.parse(result), function(index, data){
                options += "<option value='"+data.kelurahan_id+"'>"+data.kelurahan_name+"</option>"
            });
            $('.village').html(options);
            if(id){
                $('.village').removeAttr('disabled');
            } else {
                $('.village').attr('disabled', 'disabled');
            }
        });    
    });

    $('.dropdown-status li a').click(function(){
        var text = $(this).text(),
            val = $(this).attr('value'),
            btnStatus = $(this).parent().parent().parent().find(".btn-status"),
            txtStatus = $(this).parent().parent().parent().find(".status-text"),
            orderId = $(this).parent().parent().parent().parent().find(".order-id a strong").text();

        if (orderId == '') {
            orderId = $(this).parent().parent().parent().parent().parent().find(".order-id").text();
        }

        $.ajax({
            url: "/admin/order/"+orderId+"/status",
            data: {status: val},
            type: 'PUT',
            success: function(jqXHR, textStatus) {
                txtStatus.text(text);
                btnStatus.removeClass("btn-primary btn-success btn-info btn-warning btn-danger");
                if(val=='C')
                    btnStatus.addClass("btn-success");
                else if(val=='L')
                    btnStatus.addClass("btn-danger");
                else if(val=='N')
                    btnStatus.addClass("btn-warning");
                else if(val=='T' || val=='O' || val=='D')
                    btnStatus.addClass("btn-info"); 
                
                location.reload();
            },
            fail: function(jqXHR, textStatus) {
                alert( "Request failed: " + textStatus );
            }
        });
    });
});

function getBaseUrl() {
    return window.location.href.match(/^.*\//);
}