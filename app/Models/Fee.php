<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_fee';
	protected $primaryKey = 'fee_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function shop()
    {
        return $this->belongsTo('App\Models\Shop', 'shop_id');
    }

	public function area()
    {
        return $this->belongsTo('App\Models\Area', 'area_id');
    }
}