<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopMenuCat extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_kuliner_shop_menu_category';
	protected $primaryKey = 'shop_menu_category_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function menu()
    {
        return $this->hasMany('App\Models\ShopMenu', 'shop_menu_category_id');
    }

	public function shop()
    {
        return $this->belongsTo('App\Models\Shop', 'shop_id');
    }
}