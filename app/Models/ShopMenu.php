<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopMenu extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_kuliner_shop_menu';
	protected $primaryKey = 'shop_menu_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function category()
    {
        return $this->belongsTo('App\Models\ShopMenuCat', 'shop_menu_category_id');
    }
}