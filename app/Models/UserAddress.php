<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_gen_user_address';
	protected $primaryKey = 'user_address_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

	public function address()
    {
        return $this->belongsTo('App\Models\Address', 'address_id');
    }
}