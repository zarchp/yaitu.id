<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopType extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_shop_type';
	protected $primaryKey = 'shop_type_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	// public function category()
    // {
    //     return $this->hasMany('App\Models\ShopMenuCat', 'shop_id');
    // }
}