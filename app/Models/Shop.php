<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_kuliner_shop';
	protected $primaryKey = 'shop_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function category()
    {
        return $this->hasMany('App\Models\ShopMenuCat', 'shop_id');
    }

	public function area()
    {
        return $this->belongsTo('App\Models\Area', 'area_id');
    }

	public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'user_id');
	}
	
	public function type()
    {
        return $this->belongsTo('App\Models\ShopType', 'shop_type_id');
    }
}