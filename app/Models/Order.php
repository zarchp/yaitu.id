<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_kuliner_order';
	protected $primaryKey = 'order_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function items()
    {
        return $this->hasMany('App\Models\OrderMenu', 'order_id');
    }

	public function customer()
    {
        return $this->belongsTo('App\Models\User', 'customer_user_id', 'user_id');
    }

	public function agent()
    {
        return $this->belongsTo('App\Models\User', 'agent_user_id', 'user_id');
    }

	public function shop()
    {
        return $this->belongsTo('App\Models\Shop', 'shop_id');
    }

	public function area()
    {
        return $this->belongsTo('App\Models\Area', 'area_id');
    }
}