<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDepositTrans extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_gen_user_deposit_trans';
	protected $primaryKey = 'trans_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }
}