<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderMenu extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_kuliner_order_menu';
	protected $primaryKey = 'order_menu_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function menu()
    {
        return $this->belongsTo('App\Models\ShopMenu', 'shop_menu_id');
    }
}