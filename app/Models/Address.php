<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_gen_address';
	protected $primaryKey = 'address_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function kelurahan()
    {
        return $this->belongsTo('App\Models\Kelurahan', 'kelurahan_id');
    }

	public function kecamatan()
    {
        return $this->belongsTo('App\Models\Kecamatan', 'kecamatan_id');
    }

	public function kabupaten()
    {
        return $this->belongsTo('App\Models\Kabupaten', 'kabupaten_id');
    }

	public function provinsi()
    {
        return $this->belongsTo('App\Models\Provinsi', 'provinsi_id');
    }

	public function area()
    {
        return $this->belongsTo('App\Models\Area', 'area_id');
	}
	
	public function user_address()
    {
        return $this->hasOne('App\Models\UserAddress', 'address_id');
    }
}