<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'yaitu_gen_banner';
	protected $primaryKey = 'banner_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password');

	public $timestamps = false;

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

	public function shop()
    {
        return $this->belongsTo('App\Models\Shop', 'shop_id');
    }

	public function shopmenucat()
    {
        return $this->belongsTo('App\Models\ShopCat', 'shop_menu_category_id');
    }
}