<?php
    function dmytoymd($date, $limiter){
        $d = substr($date, 0, 2);
        $m = substr($date, 3, 2);
        $y = substr($date, 6);

        $result = $y.$limiter.$m.$limiter.$d;

        return $result;
    }