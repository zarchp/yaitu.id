<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Order;
use App\Models\User;
use App\Models\Shop;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = User::where('is_agent', 0)->get()->count();
        $agent = User::where('is_agent', 1)->get()->count();
        $shop = Shop::all()->count();
        $order = Order::all()->count();

        $days = date('t', mktime(0, 0, 0, date('m'), 1, date('Y')));
        for ($i=1; $i<=$days; $i++) {
            $timestamp = strtotime($i.' '.date('F Y'));
            $beginOfDay = strtotime("midnight", $timestamp);
            $endOfDay = strtotime("tomorrow", $beginOfDay) - 1;
            $new_user[$i] = User::whereBetween('register_date', [$beginOfDay, $endOfDay])->get()->count();
            $total_order[$i] = Order::whereBetween('order_date', [$beginOfDay, $endOfDay])->get()->count();
        }

        $func = function ($k) {
            return "\"".date("F")." ".$k."\"";
        };

        $new_user_key = array_combine(
            array_map($func, array_keys($new_user)),
            $new_user
        );
        $total_order_key = array_combine(
            array_map($func, array_keys($total_order)),
            $total_order
        );

        $new_user_key = implode(',', array_keys($new_user_key));
        $new_user = implode(',', $new_user);
        $total_order_key = implode(',', array_keys($total_order_key));
        $total_order = implode(',', $total_order);

        return view('admin.dashboard.index', compact('customer', 'agent', 'shop', 'order', 'new_user', 'new_user_key', 'total_order', 'total_order_key'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
