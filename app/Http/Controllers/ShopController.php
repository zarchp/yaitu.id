<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Kabupaten;
use App\Models\Provinsi;
use App\Models\Shop;
use App\Models\ShopCat;
use App\Models\User;
use App\Models\ShopType;
use App\Models\Fee;
use Toastr;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $condition = '1';
        $s_type = 0;
        if($request->input('shop_type_id')){
            $s_type = $request->input('shop_type_id');
            $condition .= " AND shop_type_id = ".$s_type;
        }

        $shop = Shop::whereRaw($condition)->orderBy('shop_id', 'desc')->get();
        $type = ShopType::all();

        return view('admin.shop.manage', compact('shop', 'type', 's_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area = Area::all();
        $user = User::where('is_agent', '1')->get();
        $shop_cat = ShopCat::all();
        $prov = Provinsi::has('kabupaten.area')->get();
        $type = ShopType::all();

        return view('admin.shop.create', compact('area', 'user', 'shop_cat', 'prov', 'type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shop = new Shop;
        if($request->has('shop_name'))
            $shop->shop_name = $request->input('shop_name');
        if($request->has('shop_category_id'))
            $shop->shop_category_id = $request->input('shop_category_id');
        if($request->has('area_id'))
            $shop->area_id = $request->input('area_id');
        if($request->has('address'))
            $shop->address = $request->input('address');
        if($request->has('created_by'))
            $shop->created_by = $request->input('created_by');
        if($request->has('is_owner'))
            $shop->is_owner = $request->input('is_owner');
        if($request->has('status'))
            $shop->status = $request->input('status');
        if($request->has('shop_type_id'))
            $shop->shop_type_id = $request->input('shop_type_id');
        if($request->has('has_delivery'))
            $shop->has_delivery = $request->input('has_delivery');
        if($request->has('service_start'))
            $shop->service_start = $request->input('service_start');
        if($request->has('service_end'))
            $shop->service_end = $request->input('service_end');
        if($request->has('deposit'))
            $shop->deposit = $request->input('deposit');

        $shop->timestamp = time();
        $shop->save();

        if($request->hasFile('photo') || $request->hasFile('banner_path')){
            if($request->hasFile('photo')){
                $path = $request->photo->store('images');
                $data[] = [
                    'name' => 'photo',
                    'contents' => fopen(storage_path('app/'.$path), 'r')
                ];
            }
            if($request->hasFile('banner_path')){
                $path = $request->banner_path->store('images');
                $data[] = [
                    'name' => 'banner_path',
                    'contents' => fopen(storage_path('app/'.$path), 'r')
                ];
            }
            $data[] = [
                'name' => '_method',
                'contents' => 'PUT'
            ];
            $client  = new \GuzzleHttp\Client();
            $req = $client->request('POST', config('yaitu.url').'shop/'.$shop->shop_id,[
                'headers' => [
                    'X-API-KEY' => auth()->user()->api_key
                ],
                'multipart' => $data,
            ]);
        }

        Toastr::success("Created", 'Success', $options = []);
        return redirect('admin/shop/edit/'.$shop->shop_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shop = Shop::find($id);
        $user = User::where('is_agent', '1')->get();
        $area = Area::where('kabupaten_id', $shop->area->kabupaten_id)->get();
        $shop_cat = ShopCat::all();
        $prov = Provinsi::has('kabupaten.area')->get();
        $kab = Kabupaten::has('area')->where('provinsi_id', $shop->area->kabupaten->provinsi_id)->get();
        $type = ShopType::all();
        $fee = Fee::where('shop_id', $id)->orderBy('fee_id', 'desc')->get();

        return view('admin.shop.edit', compact('shop', 'user', 'area', 'shop_cat', 'prov', 'kab', 'type', 'fee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shop = Shop::find($id);
        if($request->has('shop_name'))
            $shop->shop_name = $request->input('shop_name');
        if($request->has('shop_category_id'))
            $shop->shop_category_id = $request->input('shop_category_id');
        if($request->has('area_id'))
            $shop->area_id = $request->input('area_id');
        if($request->has('address'))
            $shop->address = $request->input('address');
        if($request->has('created_by'))
            $shop->created_by = $request->input('created_by');
        if($request->has('is_owner'))
            $shop->is_owner = $request->input('is_owner');
        if($request->has('status'))
            $shop->status = $request->input('status');
        if($request->has('shop_type_id'))
            $shop->shop_type_id = $request->input('shop_type_id');
        if($request->has('has_delivery'))
            $shop->has_delivery = $request->input('has_delivery');
        if($request->has('service_start'))
            $shop->service_start = $request->input('service_start');
        if($request->has('service_end'))
            $shop->service_end = $request->input('service_end');
        if($request->hasFile('photo') || $request->hasFile('banner_path')){
            if($request->hasFile('photo')){
                $path = $request->photo->store('images');
                $data[] = [
                    'name' => 'photo',
                    'contents' => fopen(storage_path('app/'.$path), 'r')
                ];
            }
            if($request->hasFile('banner_path')){
                $path = $request->banner_path->store('images');
                $data[] = [
                    'name' => 'banner_path',
                    'contents' => fopen(storage_path('app/'.$path), 'r')
                ];
            }
            $data[] = [
                'name' => '_method',
                'contents' => 'PUT'
            ];
            $client  = new \GuzzleHttp\Client();
            $req = $client->request('POST', config('yaitu.url').'shop/'.$shop->shop_id,[
                'headers' => [
                    'X-API-KEY' => auth()->user()->api_key
                ],
                'multipart' => $data,
            ]);
        }

        $shop->save();

        Toastr::success("Updated", 'Success', $options = []);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
