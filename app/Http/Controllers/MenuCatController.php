<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Provinsi;
use App\Models\Shop;
use App\Models\ShopCat;
use App\Models\ShopMenu;
use App\Models\ShopMenuCat;
use App\Models\User;
use Toastr;

class MenuCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($shop_id)
    {
        $shop = Shop::find($shop_id);

        return view('admin.shop.cat.create', compact('shop'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sc = new ShopMenuCat;
        $sc->shop_id = $request->input('shop_id');
        $sc->category_name = $request->input('category_name');
        $sc->is_active = $request->input('is_active');
        $sc->save();

        Toastr::success("Created", 'Success', $options = []);
        return redirect('admin/shop/edit/'.$sc->shop_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($shop_id, $id)
    {
        $sc = ShopMenuCat::find($id);

        return view('admin.shop.cat.edit', compact('sc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $shop_id, $id)
    {
        $sc = ShopMenuCat::find($id);
        $sc->category_name = $request->input('category_name');
        $sc->is_active = $request->input('is_active');
        $sc->save();

        Toastr::success("Updated", 'Success', $options = []);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
