<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShopCat;
use Toastr;

class ShopCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sc = ShopCat::all();

        return view('admin.shopcat.manage', compact('sc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.shopcat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $sc = new ShopCat;
        $sc->shop_category_name = $request->input('shop_category_name');
        $sc->save();
        
        Toastr::success("Created", 'Success', $options = []);
        
        return redirect('admin/shop-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sc = ShopCat::find($id);

        return view('admin.shopcat.edit', compact('sc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $sc = ShopCat::find($id);
        $sc->shop_category_name = $request->input('shop_category_name');
        $sc->save();
        
        Toastr::success("Updated", 'Success', $options = []);
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sc = ShopCat::find($id);
        $sc->delete();
        
        Toastr::success('Deleted', 'Success', $options = []);
        
        return redirect('admin/shop-category');
    }
}
