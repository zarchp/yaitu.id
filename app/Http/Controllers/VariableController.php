<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Variable;

use Toastr;

class VariableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var = Variable::all();

        return view('admin.variable.manage', compact('var'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.variable.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $var = new Variable;
        $var->variable_name = $request->input('variable_name');
        $var->type = $request->input('type');
        $var->value = $request->input('value');
        $var->save();
        
        Toastr::success("Created", 'Success', $options = []);
        
        return redirect('admin/variable');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $var = Variable::find($id);

        return view('admin.variable.edit', compact('var'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $var = Variable::find($id);
        $var->variable_name = $request->input('variable_name');
        $var->type = $request->input('type');
        $var->value = $request->input('value');
        $var->save();
        
        Toastr::success("Updated", 'Success', $options = []);
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $var = Variable::find($id);
        $var->delete();
        
        Toastr::success("Deleted", 'Success', $options = []);
        
        return redirect()->back();
    }
}
