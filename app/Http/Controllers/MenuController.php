<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Provinsi;
use App\Models\Shop;
use App\Models\ShopCat;
use App\Models\ShopMenu;
use App\Models\ShopMenuCat;
use App\Models\User;
use Toastr;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($shop_id, $sc_id)
    {
        $sc = ShopMenuCat::find($sc_id);

        return view('admin.shop.cat.menu.create', compact('sc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $shop_id, $sc_id)
    {
        $sm = new ShopMenu;
        $sm->shop_menu_category_id = $request->input('shop_menu_category_id');
        $sm->menu_name = $request->input('menu_name');
        $sm->price = $request->input('price');
        $sm->description = $request->input('description');
        $sm->is_active = $request->input('is_active');
        $sm->save();

        Toastr::success("Created", 'Success', $options = []);
        return redirect('admin/shop/'.$shop_id.'/category/edit/'.$sc_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($shop_id, $sc_id, $id)
    {
        $sm = ShopMenu::find($id);

        return view('admin.shop.cat.menu.edit', compact('sm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $shop_id, $sc_id, $id)
    {
        $sm = ShopMenu::find($id);
        $sm->shop_menu_category_id = $request->input('shop_menu_category_id');
        $sm->menu_name = $request->input('menu_name');
        $sm->price = $request->input('price');
        $sm->description = $request->input('description');
        $sm->is_active = $request->input('is_active');
        $sm->save();

        Toastr::success("Updated", 'Success', $options = []);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
