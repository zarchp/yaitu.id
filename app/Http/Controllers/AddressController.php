<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Area;
use App\Models\Address;
use App\Models\UserAddress;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use Toastr;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $user_id)
    {
        $address = new Address;
        $address->address_name = $request->input('address_name');
        $address->address = $request->input('address');
        $address->provinsi_id = $request->input('provinsi_id');
        $address->kabupaten_id = $request->input('kabupaten_id');
        // $address->kecamatan_id = $request->input('kecamatan_id');
        // $address->kelurahan_id = $request->input('kelurahan_id');
        $address->area_id = $request->input('area_id');
        $address->zip_code = $request->input('zip_code');
        $address->save();

        $ua = new UserAddress;
        $ua->user_id = $user_id;
        $ua->address_id = $address->address_id;
        $ua->is_default = $request->has('is_default')?'1':'0';
        $ua->is_agent_area_active = $request->has('is_agent_area_active')?'1':'0';
        $ua->save();

        if($ua->is_default==1)
            $this->customer_main($user_id, $address->address_id);
        if($ua->is_agent_area_active==1)
            $this->agent_main($user_id, $address->address_id);

        Toastr::success("Address created", 'Success', $options = []);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $user_id, $id)
    {
        $type = $request->input('type');
        $title = $type=='C'?'Customer':'Agent';
        $address = Address::find($id);
        $prov = Provinsi::all();
        $kab = Kabupaten::where('provinsi_id', $address->provinsi_id)->get();
        $area = Area::where('kabupaten_id', $address->kabupaten_id)->get();

        return view('admin.user.address.edit', compact('type', 'title', 'user_id', 'address', 'prov', 'kab', 'area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id, $id)
    {
        $address = Address::find($id);
        $address->address_name = $request->input('address_name');
        $address->address = $request->input('address');
        $address->provinsi_id = $request->input('provinsi_id');
        $address->kabupaten_id = $request->input('kabupaten_id');
        $address->area_id = $request->input('area_id');
        $address->zip_code = $request->input('zip_code');
        $address->save();

        $ua = UserAddress::find($address->user_address->user_address_id);
        $ua->user_id = $user_id;
        $ua->address_id = $address->address_id;
        $ua->is_default = $request->has('is_default')?'1':'0';
        $ua->is_agent_area_active = $request->has('is_agent_area_active')?'1':'0';
        $ua->save();

        if($ua->is_default==1)
            $this->customer_main($user_id, $address->address_id);
        if($ua->is_agent_area_active==1)
            $this->agent_main($user_id, $address->address_id);

        Toastr::success("Address updated", 'Success', $options = []);
        return redirect('admin/user/edit/'.$user_id.'?type='.$request->input('type'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $address_id)
    {
        $address = Address::find($address_id);
        $address->delete();

        $ua = UserAddress::where('user_id', $user_id)->where('address_id', $address_id)->first();
        $ua->delete();

        Toastr::success("Address deleted", 'Success', $options = []);
        return redirect()->back();
    }

    public function set(Request $request, $user_id, $address_id)
    {
        if($request->has('agent')){
            $this->agent_main($user_id, $address_id);
        }
        else{
            $this->customer_main($user_id, $address_id);
        }

        Toastr::success("Main address updated", 'Success', $options = []);
        return redirect()->back();
    }

    public function customer_main($user_id, $address_id)
    {
        $ua_all = UserAddress::where('user_id', $user_id)->get();
        foreach($ua_all as $u){
            $u->is_default = 0;
            $u->save();
        }

        $ua = UserAddress::where('user_id', $user_id)->where('address_id', $address_id)->first();
        $ua->is_default = 1;
        $ua->save();
    }

    public function agent_main($user_id, $address_id)
    {
        $ua_all = UserAddress::where('user_id', $user_id)->get();
        foreach($ua_all as $u){
            $u->is_agent_area_active = 0;
            $u->save();
        }

        $ua = UserAddress::where('user_id', $user_id)->where('address_id', $address_id)->first();
        $ua->is_agent_area_active = 1;
        $ua->save();
    }
}
