<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Shop;
use App\Models\ShopMenuCat;

use Toastr;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::all();

        return view('admin.banner.manage', compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shop = Shop::all();

        return view('admin.banner.create', compact('shop'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $banner = new Banner;
        $banner->name = $request->input('name');
        $banner->link = $request->input('link');
        $banner->shop_id = $request->input('shop_id');
        $banner->shop_menu_category_id = $request->input('shop_menu_category_id');
        $banner->status = $request->input('status');
        $banner->start_date = strtotime(dmytoymd($request->input('start_date'), '-'));
        $banner->end_date = strtotime(dmytoymd($request->input('end_date'), '-'));
        $banner->timestamp = time();
        $banner->save();

        if($request->hasFile('image')){
            $path = $request->image->store('images');
            $data[] = [
                'name' => 'image',
                'contents' => fopen(storage_path('app/'.$path), 'r')
            ];
            $data[] = [
                'name' => '_method',
                'contents' => 'PUT'
            ];
            $client  = new \GuzzleHttp\Client();
            $req = $client->request('POST', config('yaitu.url').'banner/'.$banner->banner_id,[
                'headers' => [
                    'X-API-KEY' => auth()->user()->api_key
                ],
                'multipart' => $data,
            ]);
        }
        
        Toastr::success("Created", 'Success', $options = []);
        
        return redirect('admin/banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $smc = null;
        $banner = Banner::find($id);
        $shop = Shop::all();
        if(isset($banner->shop_menu_category_id))
            $smc = ShopMenuCat::where('shop_id', $banner->shop_id)->get();

        return view('admin.banner.edit', compact('banner', 'shop', 'smc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::find($id);
        $banner->name = $request->input('name');
        $banner->link = $request->input('link');
        $banner->shop_id = $request->input('shop_id');
        $banner->shop_menu_category_id = $request->input('shop_menu_category_id');
        $banner->status = $request->input('status');
        $banner->start_date = strtotime(dmytoymd($request->input('start_date'), '-'));
        $banner->end_date = strtotime(dmytoymd($request->input('end_date'), '-'));
        $banner->timestamp = time();
        $banner->save();

        if($request->hasFile('image')){
            $path = $request->image->store('images');
            $data[] = [
                'name' => 'image',
                'contents' => fopen(storage_path('app/'.$path), 'r')
            ];
            $data[] = [
                'name' => '_method',
                'contents' => 'PUT'
            ];
            $client  = new \GuzzleHttp\Client();
            $req = $client->request('POST', config('yaitu.url').'banner/'.$banner->banner_id,[
                'headers' => [
                    'X-API-KEY' => auth()->user()->api_key
                ],
                'multipart' => $data,
            ]);
        }
        
        Toastr::success("Updated", 'Success', $options = []);
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner->delete();
        
        Toastr::success("Deleted", 'Success', $options = []);
        
        return redirect()->back();
    }
}
