<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Fee;
use App\Models\Kabupaten;
use App\Models\Provinsi;
use App\Models\Shop;

use Toastr;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fee = Fee::orderBy('fee_id', 'desc')->get();

        return view('admin.fee.manage', compact('fee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shop = Shop::all();
        $prov = Provinsi::has('kabupaten.area')->get();

        return view('admin.fee.create', compact('shop', 'prov'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $check = Fee::where('shop_id', $request->input('shop_id'))->where('area_id', $request->input('area_id'))->first();
        if($check){
            Toastr::error("Fee for shop and area already exist!", 'Error', $options = []);
            return redirect()->back();
        }

        $fee = new Fee;
        $fee->shop_id = $request->input('shop_id');
        $fee->area_id = $request->input('area_id');
        $fee->fee_amount = $request->input('fee_amount');
        $fee->save();
        
        Toastr::success("Created", 'Success', $options = []);
        
        if($request->input('source') == 'shop')
            return redirect()->back();
        else
            return redirect('admin/fee');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $source = $request->input('source');
        $fee = Fee::find($id);
        $shop = Shop::all();
        $prov = Provinsi::has('kabupaten.area')->get();
        $kab = Kabupaten::has('area')->where('provinsi_id', $fee->area->kabupaten->provinsi_id)->get();
        $area = Area::where('kabupaten_id', $fee->area->kabupaten->kabupaten_id)->get();

        return view('admin.fee.edit', compact('fee', 'shop', 'prov', 'kab', 'area', 'source'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Fee::where('shop_id', $request->input('shop_id'))
            ->where('area_id', $request->input('area_id'))
            ->where('fee_id', '!=', $id)
            ->first();
        if($check){
            Toastr::error("Fee for shop and area already exist!", 'Error', $options = []);
            return redirect()->back();
        }
        
        $fee = Fee::find($id);
        $fee->shop_id = $request->input('shop_id');
        $fee->area_id = $request->input('area_id');
        $fee->fee_amount = $request->input('fee_amount');
        $fee->save();
        
        Toastr::success("Updated", 'Success', $options = []);
        
        if($request->input('source') == 'shop')
            return redirect('admin/shop/edit/'.$fee->shop_id);
        else
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fee = Fee::find($id);
        $fee->delete();
        
        Toastr::success("Deleted", 'Success', $options = []);
        
        return redirect()->back();
    }
}
