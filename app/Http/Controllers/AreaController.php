<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Provinsi;
use Toastr;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area = Area::all();

        return view('admin.area.manage', compact('area'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prov = Provinsi::all();

        return view('admin.area.create', compact('prov'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('area_name');
        $kabupaten_id = $request->input('kabupaten_id');
        $status = $request->input('is_active');
        
        $area = new Area;
        $area->area_name = $name;
        $area->kabupaten_id = $kabupaten_id;
        $area->is_active = $status;
        $area->save();
        
        Toastr::success("Area berhasil ditambahkan", 'Success', $options = []);
        
        return redirect('admin/area');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = Area::find($id);
        $kab = Kabupaten::where('provinsi_id', $area->kabupaten->provinsi->provinsi_id)->get();
        $prov = Provinsi::all();

        return view('admin.area.edit', compact('area', 'kab', 'prov'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->input('area_name');
        $kabupaten_id = $request->input('kabupaten_id');
        $status = $request->input('is_active');
        
        $area = Area::find($id);
        $area->area_name = $name;
        $area->kabupaten_id = $kabupaten_id;
        $area->is_active = $status;
        $area->save();
        
        Toastr::success("Area berhasil diubah", 'Success', $options = []);
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $area = Area::find($id);
        $area->delete();
        
        Toastr::success("Area berhasil dihapus", 'Success', $options = []);
        
        return redirect('admin/area');
    }
}
