<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Area;
use App\Models\UserAddress;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use Storage;
use Toastr;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->input('type');
        $condition = '1';
        if($type=='C'){
            $title = 'Customer';
            $condition .= ' AND is_agent=0';
        }
        else{
            $title = 'Agent';
            $condition .= ' AND is_agent=1';
        }

        $user = User::whereNotIn('user_id', [1])->whereRaw($condition)->get();

        return view('admin.user.manage', compact('title', 'type', 'user', 'tes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = $request->input('type');
        $title = $type=='C'?'Customer':'Agent';

        $user = User::find($id);
        $area = Area::all();
        $ua = UserAddress::where('user_id',$id)->get();
        $prov = Provinsi::all();

        return view('admin.user.edit', compact('title', 'type', 'user', 'area', 'ua', 'prov'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if(!$user){
            Toastr::error("User not found", 'Error', $options = []);
        }

        if($request->has('fullname'))
            $user->fullname = $request->input('fullname');
        if($request->has('mobile_phone'))
            $user->mobile_phone = $request->input('mobile_phone');
        if($request->has('agent_fee'))
            $user->agent_fee = $request->input('agent_fee');
        if($request->has('service_start_hour'))
            $user->service_start_hour = $request->input('service_start_hour');
        if($request->has('service_end_hour'))
            $user->service_end_hour = $request->input('service_end_hour');
        if($request->has('area_id'))
            $user->area_id = $request->input('area_id');
        if($request->has('deposit'))
            $user->deposit = $request->input('deposit');
        if($request->hasFile('photo')){
            /*$photo = $request->file('photo');
            $folder_path = base_path('public/resource/user/'.$user->user_id);
            $filename = date('Y-m-d')."-".$photo->getClientOriginalName();
            $photo->move($folder_path, $filename);
            Image::make($folder_path.'/'.$filename)->resize(320, null, function($constraint){
                $constraint->aspectRatio();
            })->save($folder_path.'/thumb_'.$filename);
            $user->photo_path = 'resource/user/'.$user->user_id.'/thumb_'.$filename;
            $user->full_photo_path = url('resource/user/'.$user->user_id.'/thumb_'.$filename);*/
            $path = $request->photo->store('images');
            $data[] = [
                'name' => 'photo',
                'contents' => fopen(storage_path('app/'.$path), 'r')
            ];
            $client  = new \GuzzleHttp\Client();
            $req = $client->request('POST', config('yaitu.url').'users/'.$id,[
                'headers' => [
                    'X-API-KEY' => auth()->user()->api_key
                ],
                'multipart' => $data,
            ]);
            // $user = json_decode($req->getBody());
        }

        if($request->has('customer')){
            if($request->has('is_active'))
                $is_active = 1;
            else
                $is_active = 0;
            $user->is_active = $is_active;

            if($request->has('is_customer'))
                $is_customer = 1;
            else
                $is_customer = 0;
            $user->is_customer = $is_customer;
        }

        if($request->has('agent')){
            if($request->has('is_agent'))
                $is_agent = 1;
            else
                $is_agent = 0;
            $user->is_agent = $is_agent;
        }


        $user->save();

        Toastr::success("Updated", 'Success', $options = []);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();

        Toastr::success('Deleted', 'Success', $options = []);
        return redirect()->back();
    }
}
