<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Shop;
use App\Models\User;
use App\Models\ShopMenuCat;
use DB;

class AjaxController extends Controller
{
    public function regency(Request $request, $id)
    {
        if($request->ajax()){
            // $kabupaten = Kabupaten::has('area')->where('provinsi_id', $id)->get();
            $kabupaten = Kabupaten::where('provinsi_id', $id)->get();
            echo json_encode($kabupaten);
        }
    }

    public function subdistrict(Request $request, $id)
    {
        if($request->ajax()){
            $kecamatan = Kecamatan::where('kabupaten_id', $id)->get();
            echo json_encode($kecamatan);
        }
    }

    public function village(Request $request, $id)
    {
        if($request->ajax()){
            $kelurahan = DB::table('yaitu_gen_kelurahan')->where('kecamatan_id', '=', $id)->get();
            echo json_encode($kelurahan);
        }
    }

    public function area(Request $request, $id)
    {
        if($request->ajax()){
            $area = Area::where('kabupaten_id', $id)->get();
            echo json_encode($area);
        }
    }

    public function shopmenucat(Request $request, $id)
    {
        if($request->ajax()){
            $smc = ShopMenuCat::where('shop_id', $id)->get();
            echo json_encode($smc);
        }
    }

    public function owner(Request $request, $id, $shop_id=null)
    {
        if($request->ajax()){
            $shop = null;
            $owner = Shop::where('created_by', $id)
                ->where('is_owner', 1)
                ->first();

            if($shop_id!=null)
                $shop = Shop::find($shop_id);
            
            if($owner && $shop){
                $response = $shop->created_by==$id?0:1;
            }
            elseif($owner && !$shop)
                $response = 1;
            else
                $response = 0;

            echo json_encode($response);
        }
    }
}
