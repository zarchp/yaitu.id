<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Toastr;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->user_id!=1){
            Auth::logout();
            Toastr::error('Access denied', 'Error', $options = []);
            return redirect()->route('login');
        }

        return $next($request);
    }
}
