<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/privacy-policy', 'HomeController@privacy');

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin']], function() {
    Route::get('/dashboard', 'DashboardController@index');
    Route::group(['prefix' => 'user'], function() {
        Route::get('/', 'UserController@index');
        Route::get('/edit/{id}', 'UserController@edit');
        Route::put('/{id}', 'UserController@update');
        Route::delete('/{id}', 'UserController@destroy');

        Route::group(['prefix' => '{user_id}/address'], function() {
            Route::get('/edit/{id}', 'AddressController@edit');
            Route::post('/', 'AddressController@store');
            Route::delete('/{address_id}', 'AddressController@destroy');
            Route::put('{address_id}/set', 'AddressController@set');
            Route::put('/{id}', 'AddressController@update');
        });
    });

    Route::group(['prefix' => 'order'], function() {
        Route::get('/', 'OrderController@index');
        Route::get('/edit/{id}', 'OrderController@edit');
        Route::put('/{id}/status', 'OrderController@status');
        Route::put('/{id}', 'OrderController@update');
        Route::delete('/{id}', 'OrderController@destroy');
    });

    Route::group(['prefix' => 'shop'], function() {
        Route::get('/', 'ShopController@index');
        Route::get('/create', 'ShopController@create');
        Route::post('/', 'ShopController@store');
        Route::get('/edit/{id}', 'ShopController@edit');
        Route::put('/{id}', 'ShopController@update');
        Route::delete('/{id}', 'ShopController@destroy');

        Route::group(['prefix' => '{shop_id}/category'], function() {
            Route::get('/create', 'MenuCatController@create');
            Route::post('/', 'MenuCatController@store');
            Route::get('/edit/{id}', 'MenuCatController@edit');
            Route::put('/{id}', 'MenuCatController@update');
            Route::delete('/{id}', 'MenuCatController@destroy');

            Route::group(['prefix' => '{sc_id}/menu'], function() {
                Route::get('/create', 'MenuController@create');
                Route::post('/', 'MenuController@store');
                Route::get('/edit/{id}', 'MenuController@edit');
                Route::put('/{id}', 'MenuController@update');
                Route::delete('/{id}', 'MenuController@destroy');
            });
        });
    });

    Route::group(['prefix' => 'area'], function() {
        Route::get('/', 'AreaController@index');
        Route::get('/create', 'AreaController@create');
        Route::post('/', 'AreaController@store');
        Route::get('/edit/{id}', 'AreaController@edit');
        Route::put('/{id}', 'AreaController@update');
        Route::delete('/{id}', 'AreaController@destroy');
    });

    Route::group(['prefix' => 'shop-category'], function() {
        Route::get('/', 'ShopCatController@index');
        Route::get('/create', 'ShopCatController@create');
        Route::post('/', 'ShopCatController@store');
        Route::get('/edit/{id}', 'ShopCatController@edit');
        Route::put('/{id}', 'ShopCatController@update');
        Route::delete('/{id}', 'ShopCatController@destroy');
    });

    Route::group(['prefix' => 'variable'], function() {
        Route::get('/', 'VariableController@index');
        Route::get('/create', 'VariableController@create');
        Route::post('/', 'VariableController@store');
        Route::get('/edit/{id}', 'VariableController@edit');
        Route::put('/{id}', 'VariableController@update');
        Route::delete('/{id}', 'VariableController@destroy');
    });

    Route::group(['prefix' => 'fee'], function() {
        Route::get('/', 'FeeController@index');
        Route::get('/create', 'FeeController@create');
        Route::post('/', 'FeeController@store');
        Route::get('/edit/{id}', 'FeeController@edit');
        Route::put('/{id}', 'FeeController@update');
        Route::delete('/{id}', 'FeeController@destroy');
    });

    Route::group(['prefix' => 'banner'], function() {
        Route::get('/', 'BannerController@index');
        Route::get('/create', 'BannerController@create');
        Route::post('/', 'BannerController@store');
        Route::get('/edit/{id}', 'BannerController@edit');
        Route::put('/{id}', 'BannerController@update');
        Route::delete('/{id}', 'BannerController@destroy');
    });

    Route::group(['prefix' => 'qrcode'], function() {
        Route::get('/', 'QrcodeController@index');
        Route::get('/create', 'QrcodeController@create');
        Route::post('/', 'QrcodeController@store');
        Route::get('/edit/{id}', 'QrcodeController@edit');
        Route::put('/{id}', 'QrcodeController@update');
        Route::delete('/{id}', 'QrcodeController@destroy');
    });

    Route::group(['prefix' => 'ajax'], function() {
        Route::get('/regency/{id}', 'AjaxController@regency');
        Route::get('/subdistrict/{id}', 'AjaxController@subdistrict');
        Route::get('/village/{id}', 'AjaxController@village');
        Route::get('/area/{id}', 'AjaxController@area');
        Route::get('/shopmenucat/{id}', 'AjaxController@shopmenucat');
        Route::get('/owner/{id}/{shop_id?}', 'AjaxController@owner');
    });
});
