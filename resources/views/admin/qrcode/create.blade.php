@extends('layouts.admin')

@push('js')
    @include('admin.shared.address')
@endpush

@section('breadcrumb')
    <li><a href="{{ url('admin/fee') }}">Fee</a></li>
    <li class='active'>Create</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form method="POST" action="{{url('admin/fee')}}" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop</label>
                            <div class="col-sm-6">
                                <select name="shop_id" class="form-control select2" required>
                                    <option value="">-- Select Shop --</option>    
                                    @foreach($shop as $s)
                                        <option value="{{ $s->shop_id }}">{{ $s->shop_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Province</label>
                            <div class="col-sm-6">
                                <select name="provinsi_id" class="form-control select2 province" required>
                                    <option value="">-- Select Province --</option>
                                    @foreach($prov as $p)
                                        <option value="{{ $p->provinsi_id }}">{{ $p->provinsi_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Regency/City</label>
                            <div class="col-sm-6">
                                <select name="kabupaten_id" class="form-control select2 regency" required disabled>
                                    <option value="">-- Select Regency/City --</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Area</label>
                            <div class="col-sm-6">
                                <select name="area_id" class="form-control select2 area" required disabled>
                                    <option value="">-- Select Area --</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Fee</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Rp</span>
                                    <input type="text" name="fee_amount" value="" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Create" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection