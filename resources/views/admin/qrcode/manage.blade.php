@extends('layouts.admin')

@push('js')
    
@endpush

@section('breadcrumb')
    <li><a href="{{ url('admin/qrcode') }}">QR Code</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form method="GET" action="" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Text</label>
                            <div class="col-sm-8">
                                <input type="text" name="qr_code" value="{{ $qr_code!=null?$qr_code:'' }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Generate" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                {{--  ->color(156,39,176)  --}}
                    @if($qr_code!=null)
                        <img src="data:image/png;base64, {!! base64_encode(
                            QrCode::
                            format('png')
                            ->merge('http://yaitu.id/img/logo-inverse-128x128.png', .1, true)
                            ->size(200)
                            ->errorCorrection('H')
                            ->generate($qr_code)); !!} " style="width:200px;">
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection