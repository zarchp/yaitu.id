@extends('layouts.admin')

@push('js')
    @include('admin.shared.address')
@endpush

@section('breadcrumb')
    <li><a href="{{ url('admin/user?type='.$type) }}">{{ $title }}</a></li>
    <li><a href="{{ url('admin/user/edit?type='.$type, $user_id) }}">{{ $address->user_address->user->fullname }}</a></li>
    <li><a href="{{ url('admin/user/edit?type='.$type, $user_id) }}">Address</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Address</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/user/'.$user_id.'/address', $address->address_id) }}" class="form form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <input type="hidden" name="type"value="{{ $type }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Address Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="address_name" class="form-control" value="{{ $address->address_name }}" placeholder="example: office, home">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Province</label>
                            <div class="col-sm-6">
                                <select name="provinsi_id" class="form-control select2 province" required>
                                    <option value="">-- Select Province --</option>
                                    @foreach($prov as $p)
                                        <option value="{{ $p->provinsi_id }}" 
                                            {{ $p->provinsi_id==$address->provinsi_id?'selected':'' }}>{{ $p->provinsi_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Regency/City</label>
                            <div class="col-sm-6">
                                <select name="kabupaten_id" class="form-control select2 regency" required>
                                    <option value="">-- Select Regency/City --</option>
                                    @foreach($kab as $k)
                                        <option value="{{ $k->kabupaten_id }}" 
                                            {{ $k->kabupaten_id==$address->kabupaten_id?'selected':'' }}>{{ $k->kabupaten_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Area</label>
                            <div class="col-sm-6">
                                <select name="area_id" class="form-control select2 area" required>
                                    <option value="">-- Select Area --</option>
                                    @foreach($area as $a)
                                        <option value="{{ $a->area_id }}" 
                                            {{ $a->area_id==$address->area_id?'selected':'' }}>{{ $a->area_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Address</label>
                            <div class="col-sm-9">
                                <textarea name="address" class="form-control">{{ $address->address }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Zip Code</label>
                            <div class="col-sm-3">
                                <input type="text" name="zip_code" class="form-control" value="{{ $address->zip_code }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Customer Main Address</label>
                            <div class="col-sm-9">
                                <label class="switch switch-primary" style="margin-top: 7px;">
                                    <input type="checkbox" name="is_default" class="switch-input"
                                        {{ $address->user_address->is_default==1?'checked':'' }}>
                                    <span class="switch-track"></span>
                                    <span class="switch-thumb"></span>
                                </label>
                            </div>
                        </div>
                        @if($address->user_address->user->is_agent==1)
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Agent Main Address</label>
                            <div class="col-sm-9">
                                <label class="switch switch-primary" style="margin-top: 7px;">
                                    <input type="checkbox" name="is_agent_area_active" class="switch-input"
                                        {{ $address->user_address->is_agent_area_active==1?'checked':'' }}>
                                    <span class="switch-track"></span>
                                    <span class="switch-thumb"></span>
                                </label>
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary btn-sm">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>  
@endsection