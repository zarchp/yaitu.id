@extends('layouts.admin')

@push('js')
    @include('admin.shared.address')
@endpush

@section('breadcrumb')
    <li><a href="{{ url('admin/user?type='.$type) }}">{{ $title }}</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">General Information</h4>
                    <!--<div class="card-actions">
                        <button type="button" class="card-action card-toggler" title="Collapse"></button>
                        <button type="button" class="card-action card-reload" title="Reload"></button>
                        <button type="button" class="card-action card-remove" title="Remove"></button>
                    </div>
                    <strong>Basic Table (+Bootstrap Responsive Table)</strong>-->
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/user/'.$user->user_id) }}" class="form form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <input type="hidden" name="customer" value="true">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Fullname</label>
                            <div class="col-sm-9">
                                <input type="text" name="fullname" value="{{ $user->fullname }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Email</label>
                            <div class="col-sm-9">
                                <input type="text" name="email" value="{{ $user->email }}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Phone</label>
                            <div class="col-sm-9">
                                <input type="text" name="mobile_phone" value="{{ $user->mobile_phone }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Photo</label>
                            <div class="col-sm-9">
                                <img src="{{ $user->full_photo_path }}" class="img-thumbnail img-responsive">
                                <input type="file" name="photo" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Active</label>
                            <div class="col-sm-9">
                                <label class="switch switch-primary" style="margin-top: 7px;">
                                    <input type="checkbox" name="is_active" class="switch-input" {{ $user->is_active=='1'?'checked':'' }}>
                                    <span class="switch-track"></span>
                                    <span class="switch-thumb"></span>
                                </label>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Customer</label>
                            <div class="col-sm-9">
                                <label class="switch switch-primary" style="margin-top: 7px;">
                                    <input type="checkbox" name="is_customer" class="switch-input" {{ $user->is_customer=='1'?'checked':'' }}>
                                    <span class="switch-track"></span>
                                    <span class="switch-thumb"></span>
                                </label>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">API Key</label>
                            <div class="col-sm-9">
                                <input type="text" name="api_key" value="{{ $user->api_key }}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Register Date</label>
                            <div class="col-sm-9">
                                <input type="text" name="register_date" value="{{ date('d F Y H:i:s', $user->register_date) }}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Agent</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/user/'.$user->user_id) }}" class="form form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <input type="hidden" name="agent" value="true">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Agent</label>
                            <div class="col-sm-9">
                                <label class="switch switch-primary" style="margin-top: 7px;">
                                    <input type="checkbox" name="is_agent" class="switch-input" {{ $user->is_agent=='1'?'checked':'' }}>
                                    <span class="switch-track"></span>
                                    <span class="switch-thumb"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Deposit</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Rp</span>
                                    <input type="text" name="deposit" class="form-control" value="{{ $user->deposit }}" placeholder="Deposit">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Start Hour</label>
                            <div class="col-sm-3">
                                <input type="time" name="service_start_hour" value="{{ $user->service_start_hour }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">End Hour</label>
                            <div class="col-sm-3">
                                <input type="time" name="service_end_hour" value="{{ $user->service_end_hour }}" class="form-control">
                            </div>
                        </div>
                        {{--<div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Area</label>
                            <div class="col-sm-6">
                                <select name="area_id" class="form-control select2">
                                    @foreach($area as $a)
                                        <option value="{{ $a->area_id }}" 
                                            {{ $a->area_id==$user->area_id?'selected':'' }}>
                                            {{ $a->area_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>--}}
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="card-action" 
                            role="button" data-toggle="collapse" href="#add-address" 
                            aria-expanded="false" aria-controls="add-address">
                            <i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Add"></i>
                        </a>
                    </div>
                    <h4 class="card-title">Address</h4>
                </div>
                <div class="card-body">
                    <div class="collapse" id="add-address">
                        <div class="well well-sm">
                            <form method="POST" action="{{ url('admin/user/'.$user->user_id.'/address') }}" class="form form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Address Name</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="address_name" class="form-control" placeholder="example: office, home" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Province</label>
                                    <div class="col-sm-6">
                                        <select name="provinsi_id" class="form-control select2 province" required>
                                            <option value="">-- Select Province --</option>
                                            @foreach($prov as $p)
                                                <option value="{{ $p->provinsi_id }}">{{ $p->provinsi_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Regency/City</label>
                                    <div class="col-sm-6">
                                        <select name="kabupaten_id" class="form-control select2 regency" required disabled>
                                            <option value="">-- Select Regency/City --</option>

                                        </select>
                                    </div>
                                </div>
                                {{--<div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Sub-District</label>
                                    <div class="col-sm-6">
                                        <select name="kecamatan_id" class="form-control select2 subdistrict" required disabled>
                                            <option value="">-- Select Subdistrict --</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Village</label>
                                    <div class="col-sm-6">
                                        <select name="kelurahan_id" class="form-control select2 village" required disabled>
                                            <option value="">-- Select Village --</option>

                                        </select>
                                    </div>
                                </div>--}}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Area</label>
                                    <div class="col-sm-6">
                                        <select name="area_id" class="form-control select2 area" required disabled>
                                            <option value="">-- Select Area --</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Address</label>
                                    <div class="col-sm-9">
                                        <textarea name="address" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Zip Code</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="zip_code" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Customer Main Address</label>
                                    <div class="col-sm-9">
                                        <label class="switch switch-primary" style="margin-top: 7px;">
                                            <input type="checkbox" name="is_default" class="switch-input">
                                            <span class="switch-track"></span>
                                            <span class="switch-thumb"></span>
                                        </label>
                                    </div>
                                </div>
                                @if($user->is_agent==1)
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Agent Main Address</label>
                                    <div class="col-sm-9">
                                        <label class="switch switch-primary" style="margin-top: 7px;">
                                            <input type="checkbox" name="is_agent_area_active" class="switch-input">
                                            <span class="switch-track"></span>
                                            <span class="switch-thumb"></span>
                                        </label>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1"></label>
                                    <div class="col-sm-9">
                                        <input type="submit" value="Create" class="btn btn-primary btn-sm">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <ul class="list-unstyled">
                        @forelse ($ua as $u)
                            <li>
                                <address>
                                    <strong>{{ $u->address->address_name }}</strong>
                                    @if($u->is_default==1)
                                        <i class="fa fa-user bg-primary-inverse" 
                                            data-toggle="tooltip" data-placement="top" title="Customer Main Address"></i>
                                        &nbsp;
                                    @endif
                                    @if($user->is_agent==1 && $u->is_agent_area_active==1)
                                        <i class="fa fa-user-secret bg-primary-inverse" 
                                            data-toggle="tooltip" data-placement="top" title="Agent Main Address"></i>
                                    @endif
                                    <br>
                                    {{ $u->address->address }}<br>
                                    {{ $u->address->area->area_name }}, 
                                    {{-- {{ title_case($u->address->kelurahan->kelurahan_name) }}, 
                                    {{ title_case($u->address->kecamatan->kecamatan_name) }}, --}}
                                    {{ title_case($u->address->kabupaten->kabupaten_name) }}
                                    {{ $u->address->zip_code!=0?$u->address->zip_code:'' }}<br>
                                    {{ title_case($u->address->provinsi->provinsi_name) }}<br>
                                    <div class="button">
                                        <div class="pull-left">
                                            @if($u->is_default==0)
                                                <a href="{{ url('admin/user/'.$u->user_id.'/address/'.$u->address_id.'/set') }}" 
                                                    class="btn btn-primary btn-xs" data-method="put" data-token="{{ csrf_token() }}">
                                                    Set as Customer Address
                                                </a>
                                                &nbsp;
                                            @endif
                                            @if($user->is_agent==1 && $u->is_agent_area_active==0)
                                                <a href="{{ url('admin/user/'.$u->user_id.'/address/'.$u->address_id.'/set?agent=1') }}" 
                                                    class="btn btn-info btn-xs" data-method="put" data-token="{{ csrf_token() }}">
                                                    Set as Agent Address
                                                </a>
                                            @endif
                                        </div>
                                        <div class="pull-right">
                                            <!--<a href="#" class="btn btn-success btn-xs text-right">Edit</a>
                                            <a href="#" class="btn btn-danger btn-xs">Delete</a>-->
                                            <a href="{{ url('admin/user/'.$u->user_id.'/address/edit/'.$u->address_id.'?type=C') }}" class="btn btn-xs btn-success" 
                                                data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="{{ url('admin/user/'.$u->user_id.'/address/'.$u->address_id) }}" class="btn btn-xs btn-danger"
                                                data-toggle="tooltip" data-placement="top" title="Delete"
                                                data-method="DELETE" data-token="{{csrf_token()}}" 
                                                data-confirm="Are you sure want to delete this data?">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!--<abbr title="Phone">P:</abbr> (123) 456-7890-->
                                </address>
                            </li>
                        @empty
                            <li>
                                No data
                            </li>
                        @endforelse
                    </ul>
                    <div class="table-responsive">
                        <table class="table table-middle table-striped table-hover">
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>  
@endsection