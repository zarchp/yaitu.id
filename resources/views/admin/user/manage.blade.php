@extends('layouts.admin')

@section('breadcrumb')
    <li class='active'>{{ $title }}</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="demo-dynamic-tables-2" class="table table-middle">
                            <thead>
                                <tr>
                                    <th class="hidden">ID</th>
                                    <th>Fullname</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Area</th>
                                    @if($type=='A')
                                        <th>Fee</th>
                                    @endif
                                    <th>Timestamp</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($user as $u)
                                <tr>
                                    <td class="hidden">{{ $u->user_id }}</td>
                                    <td data-order="{{ $u->fullname }}">
                                        <a href="{{ url('admin/user/edit', [$u->user_id]) }}?type={{ $type }}">
                                            <span class="icon-with-child m-r">
                                                <img class="circle" width="36" height="36" src="{{ $u->full_photo_path or asset('img/default-avatar.png') }}" alt="">
                                            </span>
                                            <strong>{{ $u->fullname }}</strong>
                                        </a>
                                    </td>
                                    <td class="maw-320">
                                        <span class="truncate">{{ $u->email }}</span>
                                    </td>
                                    <td>{{ $u->mobile_phone }}</td>
                                    <td data-order="1">
                                        @if($u->is_active=='1')
                                            <span class="label label-success label-pill">Active</span>
                                        @else
                                            <span class="label label-danger label-pill">Disabled</span>
                                        @endif
                                    </td>
                                    <td data-order="3">{{ $u->area->area_name or '-' }}</td>
                                    @if($type=='A')
                                        <td data-order="1">Rp {{ $u->agent_fee }}</td>
                                    @endif
                                    <td data-order="3">
                                        {{ date('d-m-Y', $u->register_date) }}<!--<br>
                                        {{ date('H:i:s', $u->timestamp) }}-->
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/user/edit', [$u->user_id]) }}?type={{ $type }}" class="btn btn-xs btn-success" 
                                            data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('admin/user', [$u->user_id]) }}" class="btn btn-xs btn-danger"
                                            data-toggle="tooltip" data-placement="top" title="Delete"
                                            data-method="DELETE" data-token="{{csrf_token()}}" 
                                            data-confirm="Are you sure want to delete this data?">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection