@extends('layouts.admin')

@section('breadcrumb')
    <li><a href="{{ url('admin/shop') }}">Shop</a></li>
    <li><a href="{{ url('admin/shop/edit', $sc->shop->shop_id) }}">{{ $sc->shop->shop_name }}</a></li>
    <li><a href="{{ url('admin/shop/edit', $sc->shop->shop_id) }}">Category</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">General Information</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/shop/'.$sc->shop->shop_id.'/category/'.$sc->shop_menu_category_id) }}" class="form form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <input type="hidden" name="shop_id" value="{{ $sc->shop->shop_id }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="shop_name" value="{{ $sc->shop->shop_name }}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Category Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="category_name" value="{{ $sc->category_name }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Status</label>
                            <div class="col-sm-3">
                                <select name="is_active" class="form-control select2">
                                    <option value="1" {{ $sc->is_active==1?'selected':'' }}>Active</option>
                                    <option value="0" {{ $sc->is_active==0?'selected':'' }}>Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Menu</h4>
                </div>
                <div class="card-body">
                    <a href="{{ url('admin/shop/'.$sc->shop->shop_id.'/category/'.$sc->shop_menu_category_id.'/menu/create') }}" 
                        class="btn btn-primary m-b-sm"><i class="fa fa-add"></i> Create New</a>
                        
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category Name</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Menu</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($sc->menu as $m)
                                <tr>
                                    <td>
                                        <a href="{{ url('admin/shop/'.$sc->shop->shop_id.'/category/'.$sc->shop_menu_category_id.'/menu/edit/'.$m->shop_menu_id) }}">
                                            {{ $m->shop_menu_id }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/shop/'.$sc->shop->shop_id.'/category/'.$sc->shop_menu_category_id.'/menu/edit/'.$m->shop_menu_id) }}">
                                            {{ $m->menu_name }}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        @if($m->is_active=='1')
                                            <span class="label label-success label-pill">Active</span>
                                        @else
                                            <span class="label label-danger label-pill">Disabled</span>
                                        @endif
                                    </td>
                                    <td class="text-center">Rp {{ number_format($m->price, 0, ',', '.') }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>  
@endsection