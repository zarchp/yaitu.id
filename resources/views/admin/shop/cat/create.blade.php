@extends('layouts.admin')

@section('breadcrumb')
    <li><a href="{{ url('admin/shop') }}">Shop</a></li>
    <li><a href="{{ url('admin/shop/edit', $shop->shop_id) }}">{{ $shop->shop_name }}</a></li>
    <li><a href="{{ url('admin/shop/edit', $shop->shop_id) }}">Category</a></li>
    <li class='active'>Create</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">General Information</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/shop/'.$shop->shop_id.'/category') }}" class="form form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="shop_id" value="{{ $shop->shop_id }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="shop_name" value="{{ $shop->shop_name }}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Category Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="category_name" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Status</label>
                            <div class="col-sm-3">
                                <select name="is_active" class="form-control select2">
                                    <option value="1">Active</option>
                                    <option value="0">Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Create" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            
        </div>
    </div>  
@endsection