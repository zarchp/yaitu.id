@extends('layouts.admin')

@section('breadcrumb')
    <li><a href="{{ url('admin/shop') }}">Shop</a></li>
    <li><a href="{{ url('admin/shop/edit', $sm->category->shop->shop_id) }}">{{ $sm->category->shop->shop_name }}</a></li>
    <li><a href="{{ url('admin/shop/edit', $sm->category->shop->shop_id) }}">Category</a></li>
    <li><a href="{{ url('admin/shop/'.$sm->category->shop->shop_id.'/category/edit', $sm->shop_menu_category_id) }}">{{ $sm->category->category_name }}</a></li>
    <li><a href="{{ url('admin/shop/'.$sm->category->shop->shop_id.'/category/edit', $sm->shop_menu_category_id) }}">Menu</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">General Information</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/shop/'.$sm->category->shop->shop_id.'/category/'.$sm->category->shop_menu_category_id.'/menu/'.$sm->shop_menu_id) }}" class="form form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <input type="hidden" name="shop_menu_category_id" value="{{ $sm->shop_menu_category_id }}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="shop_name" value="{{ $sm->category->shop->shop_name }}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Category Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="category_name" value="{{ $sm->category->category_name }}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Menu Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="menu_name" value="{{ $sm->menu_name }}" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Price</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Rp</span>
                                    <input type="text" name="price" value="{{ $sm->price }}" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Description</label>
                            <div class="col-sm-8">
                                <textarea name="description" class="form-control" row="2" required>{{ $sm->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Status</label>
                            <div class="col-sm-3">
                                <select name="is_active" class="form-control select2">
                                    <option value="1" {{ $sm->is_active==1?'selected':'' }}>Active</option>
                                    <option value="0" {{ $sm->is_active==0?'selected':'' }}>Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            
        </div>
    </div>  
@endsection