@extends('layouts.admin')

@push('js')
    @include('admin.shared.address')
    @include('admin.shared.owner')
    @include('admin.shared.shop')
@endpush

@section('breadcrumb')
    <li><a href="{{ url('admin/shop') }}">Shop</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">General Information</h4>
                    <!--<div class="card-actions">
                        <button type="button" class="card-action card-toggler" title="Collapse"></button>
                        <button type="button" class="card-action card-reload" title="Reload"></button>
                        <button type="button" class="card-action card-remove" title="Remove"></button>
                    </div>
                    <strong>Basic Table (+Bootstrap Responsive Table)</strong>-->
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/shop/'.$shop->shop_id) }}" class="form form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <input type="hidden" name="shop_id" class="shop_id" value="{{ $shop->shop_id }}">
                        <input type="hidden" name="customer" value="true">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Type</label>
                            <div class="col-sm-5">
                                <select name="shop_type_id" class="form-control select2">
                                    <option value="">-- Select Type --</option>
                                    @foreach($type as $t)
                                        <option value="{{ $t->shop_type_id }}" {{ $shop->shop_type_id==$t->shop_type_id?'selected':'' }}>{{ $t->shop_type_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Category</label>
                            <div class="col-sm-5">
                                <select name="shop_category_id" class="form-control select2" disabled>
                                    <option value="0">-- Select Category --</option>
                                    @foreach($shop_cat as $sc)
                                        <option value="{{ $sc->shop_category_id }}" {{ $shop->shop_category_id==$sc->shop_category_id?'selected':'' }}>
                                            {{ $sc->shop_category_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="shop_name" value="{{ $shop->shop_name }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Created by</label>
                            <div class="col-sm-6">
                                {{--  <input type="text" name="created_by_email" value="{{ $shop->creator->email }}" class="form-control" readonly>  --}}
                                <select name="created_by" class="form-control select2 owner">
                                    @foreach($user as $u)
                                        <option value="{{ $u->user_id }}" {{ $shop->created_by==$u->user_id?'selected':'' }}>
                                            {{ $u->email }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Owner</label>
                            <div class="col-sm-3">
                                <input type="hidden" name="is_owner" class="hdn_owner" disabled="disabled">
                                <select name="is_owner" class="form-control is_owner">
                                    <option value="0" {{ $shop->is_owner==0?'selected':'' }}>No</option>
                                    <option value="1" {{ $shop->is_owner==1?'selected':'' }}>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Status</label>
                            <div class="col-sm-3">
                                <select name="status" class="form-control is_owner">
                                    <option value="A" {{ $shop->status=='A'?'selected':'' }}>Active</option>
                                    <option value="D" {{ $shop->status=='D'?'selected':'' }}>Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Province</label>
                            <div class="col-sm-6">
                                <select name="provinsi_id" class="form-control select2 province" required>
                                    <option value="">-- Select Province --</option>
                                    @foreach($prov as $p)
                                        <option value="{{ $p->provinsi_id }}" 
                                            {{ $shop->area->kabupaten->provinsi_id==$p->provinsi_id?'selected':'' }}>
                                            {{ $p->provinsi_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Regency/City</label>
                            <div class="col-sm-6">
                                <select name="kabupaten_id" class="form-control select2 regency" required>
                                    <option value="">-- Select Regency/City --</option>
                                    @foreach($kab as $k)
                                        <option value="{{ $k->kabupaten_id }}" 
                                            {{ $shop->area->kabupaten_id==$k->kabupaten_id?'selected':'' }}>
                                            {{ $k->kabupaten_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Area</label>
                            <div class="col-sm-6">
                                <select name="area_id" class="form-control select2 area" required>
                                    @foreach($area as $a)
                                        <option value="{{ $a->area_id }}" 
                                            {{ $a->area_id==$shop->area_id?'selected':'' }}>
                                            {{ $a->area_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Address</label>
                            <div class="col-sm-8">
                                <textarea name="address" class="form-control" row="3">{{ $shop->address }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Delivery</label>
                            <div class="col-sm-3">
                                <select name="has_delivery" class="form-control select2">
                                    <option value="0" {{ $shop->has_delivery==0?'selected':'' }}>No</option>
                                    <option value="1" {{ $shop->has_delivery==1?'selected':'' }}>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Service Hour</label>
                            <div class="col-sm-3">
                                <input type="time" name="service_start" value="{{ $shop->service_start }}" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="time" name="service_end" value="{{ $shop->service_end }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Photo</label>
                            <div class="col-sm-4">
                                <img src="{{ $shop->full_photo_path }}" class="img-thumbnail img-responsive">
                                <input type="file" name="photo" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Banner</label>
                            <div class="col-sm-4">
                                <img src="{{ $shop->banner_path }}" class="img-thumbnail img-responsive">
                                <input type="file" name="banner_path" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="card-action" 
                            role="button" data-toggle="collapse" href="#createFee" 
                            aria-expanded="false" aria-controls="createFee">
                            <i class="fa fa-plus" data-toggle="tooltip" data-placement="top" title="Add"></i>
                        </a>
                    </div>
                    <h4 class="card-title">Delivery Fee</h4>
                </div>
                <div class="card-body">
                    <div class="collapse" id="createFee">
                        <div class="well">
                            <form method="POST" action="{{url('admin/fee')}}" class="form-horizontal">
                                {{ csrf_field() }}
                                <input type="hidden" name="source" class="form-control" value="shop">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Shop</label>
                                    <div class="col-sm-6">
                                        <input type="hidden" name="shop_id" class="form-control" value="{{ $shop->shop_id }}">
                                        <input type="text" name="shop_name" class="form-control" value="{{ $shop->shop_name }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Province</label>
                                    <div class="col-sm-6">
                                        <select name="provinsi_id" class="form-control select2 province" required>
                                            <option value="">-- Select Province --</option>
                                            @foreach($prov as $p)
                                                <option value="{{ $p->provinsi_id }}">{{ $p->provinsi_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Regency/City</label>
                                    <div class="col-sm-6">
                                        <select name="kabupaten_id" class="form-control select2 regency" required disabled>
                                            <option value="">-- Select Regency/City --</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Area</label>
                                    <div class="col-sm-6">
                                        <select name="area_id" class="form-control select2 area" required disabled>
                                            <option value="">-- Select Area --</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1">Fee</label>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Rp</span>
                                            <input type="text" name="fee_amount" value="" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-1"></label>
                                    <div class="col-sm-9">
                                        <input type="submit" value="Create" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Area</th>
                                    <th>Fee</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($fee as $f)
                                <tr>
                                    <td>
                                        <a href="{{ url('admin/fee/edit?source=shop', [$f->fee_id]) }}">
                                            {{ $f->fee_id }}
                                        </a>
                                    </td>
                                    <td>
                                        {{ $f->area->area_name or '-' }}
                                    </td>
                                    <td>Rp {{ $f->fee_amount }}</td>
                                    <td class="text-center">
                                        <a href="{{ url('admin/fee/edit?source=shop', [$f->fee_id]) }}" class="btn btn-xs btn-success" 
                                            data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('admin/fee', [$f->fee_id]) }}" class="btn btn-xs btn-danger"
                                            data-toggle="tooltip" data-placement="top" title="Delete"
                                            data-method="DELETE" data-token="{{csrf_token()}}" 
                                            data-confirm="Are you sure want to delete this data?">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Menu Category</h4>
                </div>
                <div class="card-body">
                    <a href="{{ url('admin/shop/'.$shop->shop_id.'/category/create') }}" class="btn btn-primary m-b-sm"><i class="fa fa-add"></i> Create New</a>
                        
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category Name</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Menu</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($shop->category as $c)
                                <tr>
                                    <td>
                                        <a href="{{ url('admin/shop/'.$shop->shop_id.'/category/edit', [$c->shop_menu_category_id]) }}">
                                            {{ $c->shop_menu_category_id }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/shop/'.$shop->shop_id.'/category/edit', [$c->shop_menu_category_id]) }}">
                                            {{ $c->category_name }}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        @if($c->is_active=='1')
                                            <span class="label label-success label-pill">Active</span>
                                        @else
                                            <span class="label label-danger label-pill">Disabled</span>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $c->menu->count() }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>  
@endsection