@extends('layouts.admin')

@push('js')
    @include('admin.shared.address')
    @include('admin.shared.owner')
    @include('admin.shared.shop')
@endpush

@section('breadcrumb')
    <li><a href="{{ url('admin/shop') }}">Shop</a></li>
    <li class='active'>Create</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">General Information</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/shop') }}" class="form form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="customer" value="true">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Type</label>
                            <div class="col-sm-5">
                                <select name="shop_type_id" class="form-control select2" required>
                                    <option value="">-- Select Type --</option>
                                    @foreach($type as $t)
                                        <option value="{{ $t->shop_type_id }}">{{ $t->shop_type_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Category</label>
                            <div class="col-sm-5">
                                <select name="shop_category_id" class="form-control select2" required disabled>
                                    <option value="">-- Select Category --</option>
                                    @foreach($shop_cat as $sc)
                                        <option value="{{ $sc->shop_category_id }}">
                                            {{ $sc->shop_category_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="shop_name" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Created by</label>
                            <div class="col-sm-6">
                                <select name="created_by" class="form-control select2 owner">
                                    @foreach($user as $u)
                                        <option value="{{ $u->user_id }}">
                                            {{ $u->email }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Owner</label>
                            <div class="col-sm-3">
                                <input type="hidden" name="is_owner" class="hdn_owner" disabled="disabled">
                                <select name="is_owner" class="form-control is_owner">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Status</label>
                            <div class="col-sm-3">
                                <select name="status" class="form-control">
                                    <option value="A">Active</option>
                                    <option value="D">Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Province</label>
                            <div class="col-sm-6">
                                <select name="provinsi_id" class="form-control select2 province" required>
                                    <option value="">-- Select Province --</option>
                                    @foreach($prov as $p)
                                        <option value="{{ $p->provinsi_id }}">{{ $p->provinsi_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Regency/City</label>
                            <div class="col-sm-6">
                                <select name="kabupaten_id" class="form-control select2 regency" required disabled>
                                    <option value="">-- Select Regency/City --</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Area</label>
                            <div class="col-sm-6">
                                <select name="area_id" class="form-control select2 area" required disabled>
                                    <option value="">-- Select Area --</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Address</label>
                            <div class="col-sm-8">
                                <textarea name="address" class="form-control" row="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Delivery</label>
                            <div class="col-sm-3">
                                <select name="has_delivery" class="form-control select2">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Service Hour</label>
                            <div class="col-sm-3">
                                <input type="time" name="service_start" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="time" name="service_end" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Photo</label>
                            <div class="col-sm-6">
                                <input type="file" name="photo" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Banner</label>
                            <div class="col-sm-6">
                                <input type="file" name="banner_path" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Create" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            
        </div>
    </div>  
@endsection