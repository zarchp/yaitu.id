@extends('layouts.admin')

@section('breadcrumb')
    <li class='active'>Shop</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-2 m-b-md">
            <a href="{{ url('admin/shop/create') }}" class="btn btn-primary"><i class="fa fa-add"></i> Create New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <form method="GET" action="" class="form-inline m-b-md">
                        <div class="form-group">
                            <label class="sr-only">Shop Type</label>
                            <select name="shop_type_id" class="form-control select2">
                                <option value="0">-- All Type --</option>
                                @foreach($type as $t)
                                    <option value="<?= $t->shop_type_id ?>" {{ $s_type==$t->shop_type_id?'selected':'' }}>
                                        <?= $t->shop_type_name ?>
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm m-l-sm">Filter</button>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable-all">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Shop Name</th>                                   
                                    <th>Status</th>  
                                    <th>Created By</th>
                                    <th>Owner</th>
                                    <th>Area</th>
                                    <th>Type</th>                                   
                                    <!--<th>Action</th>-->
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($shop as $s)
                                <tr>
                                    <td class="shop-id td-hover">
                                        <a href="{{ url('admin/shop/edit', [$s->shop_id]) }}">
                                            <strong>{{ $s->shop_id }}</strong>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/shop/edit', [$s->shop_id]) }}">
                                            <span class="icon-with-child m-r">
                                                <img class="circle" width="36" height="36" src="{{ $s->full_photo_path or asset('img/default-shop.png') }}" alt="">
                                            </span>
                                            <strong>{{ $s->shop_name }}</strong>
                                        </a>
                                    </td>
                                    <td>
                                        @if($s->status=='A')
                                            <span class="label label-success label-pill">Active</span>
                                        @else
                                            <span class="label label-danger label-pill">Disabled</span>
                                        @endif
                                    </td>
                                    <td>{{ $s->creator->email or '-' }}</td>
                                    <td>
                                        @if($s->is_owner=='1')
                                            <span class="label label-success label-pill">Yes</span>
                                        @else
                                            <span class="label label-danger label-pill">No</span>
                                        @endif
                                    </td>
                                    <td>{{ $s->area->area_name or '-' }}</td>
                                    <td>{{ $s->type->shop_type_name }}</td>
                                    {{--<td>
                                        <a href="{{ url('admin/shop/edit', [$s->shop_id]) }}" class="btn btn-xs btn-success" 
                                            data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('admin/shop', [$s->shop_id]) }}" class="btn btn-xs btn-danger"
                                            data-toggle="tooltip" data-placement="top" title="Delete"
                                            data-method="DELETE" data-token="{{csrf_token()}}" 
                                            data-confirm="Are you sure want to delete this data?">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>--}}
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection