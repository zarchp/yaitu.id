@extends('layouts.admin')

@section('breadcrumb')
    <li class='active'>Banner</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-2 m-b-md">
            <a href="{{ url('admin/banner/create') }}" class="btn btn-primary"><i class="fa fa-add"></i> Create New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Banner</th>
                                    <th>Shop</th>
                                    <th>Active</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($banner as $b)
                                <tr>
                                    <td class="order-id td-hover">
                                        <a href="{{ url('admin/banner/edit', [$b->banner_id]) }}">
                                            <strong>{{ $b->banner_id }}</strong>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/banner/edit', [$b->banner_id]) }}">
                                            <span class="icon-with-child m-r">
                                                <img class="img-rounded" width="60" height="36" src="{{ $b->image_path or asset('img/no_image.jpeg') }}" alt="">
                                            </span>
                                            <strong>{{ $b->name }}</strong>
                                        </a>
                                    </td>
                                    <td><a href="{{ url('admin/shop/edit', [$b->shop_id]) }}">{{ $b->shop->shop_name }}</a></td>
                                    <td>{{ date('d/m/Y', $b->start_date) }} <strong>-</strong> {{ date('d/m/Y', $b->end_date) }}</td>
                                    <td>
                                        @if($b->status=='A')
                                            <span class="label label-success label-pill">Active</span>
                                        @else
                                            <span class="label label-danger label-pill">Disabled</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/banner/edit', [$b->banner_id]) }}" class="btn btn-xs btn-success" 
                                            data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('admin/banner', [$b->banner_id]) }}" class="btn btn-xs btn-danger"
                                            data-toggle="tooltip" data-placement="top" title="Delete"
                                            data-method="DELETE" data-token="{{csrf_token()}}" 
                                            data-confirm="Are you sure want to delete this data?">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection