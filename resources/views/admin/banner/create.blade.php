@extends('layouts.admin')

@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script>
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });
    </script>
    @include('admin.shared.banner')
@endpush

@section('breadcrumb')
    <li><a href="{{ url('admin/banner') }}">Banner</a></li>
    <li class='active'>Create</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form method="POST" action="{{url('admin/banner')}}" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="name" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Image</label>
                            <div class="col-sm-6">
                                <input type="file" name="image" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Type</label>
                            <div class="col-sm-4">
                                <select name="link" class="form-control link" required>  
                                    <option value="S">Shop</option>
                                    <option value="C">Menu Category</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop</label>
                            <div class="col-sm-6">
                                <select name="shop_id" class="form-control select2 shop" required>
                                    <option value="">-- Select Shop --</option>
                                    @foreach($shop as $s)
                                        <option value="{{ $s->shop_id }}">{{ $s->shop_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Menu Category</label>
                            <div class="col-sm-6">
                                <select name="shop_menu_category" class="form-control select2 shopmenucat" required disabled>
                                    <option value="">-- Select Menu Category --</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Status</label>
                            <div class="col-sm-4">
                                <select name="status" class="form-control link" required>  
                                    <option value="A">Active</option>
                                    <option value="D">Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Active from</label>
                            <div class="col-sm-4">
                                <input type="date" name="start_date" value="" class="form-control datepicker" required>
                            </div>
                            <label class="col-sm-1 control-label" for="form-control-1">to</label>
                            <div class="col-sm-4">
                                <input type="date" name="end_date" value="" class="form-control datepicker" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Create" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection