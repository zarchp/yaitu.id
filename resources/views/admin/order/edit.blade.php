@extends('layouts.admin')

@section('breadcrumb')
    <li><a href="{{ url('admin/order') }}">Order</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Order Information</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('admin/order/'.$order->order_id) }}" class="form form-horizontal" enctype="multipart/form-data">
                        <span class="order-id hidden">{{ $order->order_id }}</span>
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Status :</label>
                            <div class="col-sm-8 dropdown">
                                <button id="dLabel" type="button" class="btn 
                                @if($order->status=='N')
                                    btn-warning
                                @elseif($order->status=='T' || $order->status=='O' || $order->status=='D')
                                    btn-info
                                @elseif($order->status=='C')
                                    btn-success
                                @else
                                    btn-danger
                                @endif
                                btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($order->status=='N')
                                        New
                                    @elseif($order->status=='T')
                                        Taken
                                    @elseif($order->status=='O')
                                        Menu Ordered
                                    @elseif($order->status=='D')
                                        Being Delivered
                                    @elseif($order->status=='C')
                                        Completed
                                    @else
                                        Canceled
                                    @endif
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-status" aria-labelledby="dLabel">
                                    <li><a href="#" value="N">New</a></li>
                                    <li><a href="#" value="T">Taken</a></li>
                                    <li><a href="#" value="O">Menu Ordered</a></li>
                                    <li><a href="#" value="D">Being Delivered</a></li>
                                    <li><a href="#" value="C">Completed</a></li>
                                    <li><a href="#" value="L">Canceled</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Shop :</label>
                            <div class="col-sm-8">
                                <label class="control-label" style="font-weight:normal;">{{ $order->shop->shop_name }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Area :</label>
                            <div class="col-sm-8">
                                <label class="control-label" style="font-weight:normal;">{{ $order->area->area_name }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Order Date :</label>
                            <div class="col-sm-8">
                                <label class="control-label" style="font-weight:normal;">{{ date('H:i:s d F Y', $order->order_date) }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Agent :</label>
                            <div class="col-sm-8">
                                <label class="control-label" style="font-weight:normal;">
                                    <a href="@if(isset($order->agent->user_id)){{ url('admin/user/edit?type=A', [$order->agent->user_id]) }}@else#@endif">
                                        {{ $order->agent->fullname or "-" }}
                                    </a>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Order Taken :</label>
                            <div class="col-sm-8">
                                <label class="control-label" style="font-weight:normal;">
                                    @if(isset($order->order_taken))
                                        {{ date('H:i:s d F Y', $order->order_taken) }}
                                    @else
                                        -
                                    @endif
                                </label>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>-->
                    </form>
                </div>
            </div>
             <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Customer</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="#" class="form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Fullname :</label>
                            <div class="col-sm-8">
                                <label class="control-label" style="font-weight:normal;">
                                    <a href="{{ url('admin/user/edit?type=C', [$order->customer->user_id]) }}">{{ $order->customer->fullname or "-" }}</a>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Phone :</label>
                            <div class="col-sm-8">
                                <label class="control-label" style="font-weight:normal;">{{ $order->customer->mobile_phone or "-" }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-control-1">Address :</label>
                            <div class="col-sm-8">
                                <label class="control-label" style="font-weight:normal;text-align:left;">
                                    {{ $address->address }}<br>
                                    {{ $address->area->area_name }}, 
                                    {{ title_case($address->kabupaten->kabupaten_name) }}
                                    {{ $address->zip_code!=0?$address->zip_code:'' }}<br>
                                    {{ title_case($address->provinsi->provinsi_name) }}<br>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Items</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-middle">
                            <thead>
                                <tr>
                                    <th>Menu</th>
                                    <th>Price</th>
                                    <th class="text-center">Qty</th>
                                    <th>Subtotal</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($order->items as $o)
                                <tr>
                                    <td>
                                        @if($o->shop_menu_id!=0)
                                            <a href="{{ url('admin/shop/menu', $o->shop_menu_id) }}">
                                                {{ $o->menu->menu_name }}
                                            </a>
                                        @else
                                            {{ $o->menu_name }}
                                        @endif
                                    </td>
                                    <td>Rp {{ number_format($o->price, 0, ',', '.') }}</td>
                                    <td class="text-center">{{ $o->qty }}</td>
                                    <td class="text-right">Rp {{ number_format($o->price*$o->qty, 0, ',', '.') }}</td>
                                    <td>
                                        <button class="btn btn-primary btn-xs" data-toggle="tooltip" 
                                            data-placement="bottom" title="{{ $o->note }}">
                                            <i class="fa fa-file-text-o"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                                <tr>
                                    <td></td>
                                    <td class="text-right">Agent Commision :</td>
                                    <td></td>
                                    <td class="text-right">Rp {{ number_format($order->agent_commision, 0, ',', '.') }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="text-right">Yaitu Commision :</td>
                                    <td></td>
                                    <td class="text-right">Rp {{ number_format($order->yaitu_commision, 0, ',', '.') }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="text-right"><strong>Total :</strong></td>
                                    <td></td>
                                    <td class="text-right"><strong>Rp {{ number_format($order->total_price, 0, ',', '.') }}</strong></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>  
@endsection