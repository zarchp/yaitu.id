@extends('layouts.admin')

@push('js')
    <script>
        $(function(){
            $('.order-id').click(function(e) {
                var orderId = $(this).find("strong").text();
                window.location.href = "{{ url('admin/order/edit') }}/"+orderId;
            });
        });
    </script>
@endpush

@section('breadcrumb')
    <li class='active'>Order</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable-all order">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>Timestamp</th>
                                    <th>Customer</th>
                                    <th>Price</th>                                    
                                    <th>Shop</th>
                                    <th>Area</th>
                                    <!--<th>Action</th>-->
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($order as $o)
                                <tr>
                                    <td class="order-id td-hover">
                                        <a href="{{ url('admin/order/edit', [$o->order_id]) }}">
                                            <strong>{{ $o->order_id }}</strong>
                                        </a>
                                    </td>
                                    <td class="dropdown">
                                        <button id="dLabel" type="button" class="btn 
                                        @if($o->status=='N')
                                            btn-warning
                                        @elseif($o->status=='T' || $o->status=='O' || $o->status=='D')
                                            btn-info
                                        @elseif($o->status=='C')
                                            btn-success
                                        @else
                                            btn-danger
                                        @endif
                                        btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            @if($o->status=='N')
                                                New
                                            @elseif($o->status=='T')
                                                Taken
                                            @elseif($o->status=='O')
                                                Menu Ordered
                                            @elseif($o->status=='D')
                                                Being Delivered
                                            @elseif($o->status=='C')
                                                Completed
                                            @else
                                                Canceled
                                            @endif
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-status" aria-labelledby="dLabel">
                                            <li><a href="#" value="N">New</a></li>
                                            <li><a href="#" value="T">Taken</a></li>
                                            <li><a href="#" value="O">Menu Ordered</a></li>
                                            <li><a href="#" value="D">Being Delivered</a></li>
                                            <li><a href="#" value="C">Completed</a></li>
                                            <li><a href="#" value="L">Canceled</a></li>
                                        </ul>
                                    </td>
                                    <td>
                                        {{ date('d-m-Y', $o->order_date) }}<br>
                                        {{ date('H:i:s', $o->order_date) }}
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/user/edit', [$o->customer_user_id]) }}">
                                            <span class="icon-with-child m-r">
                                                <img class="circle" width="36" height="36" src="{{ $o->customer->full_photo_path or asset('img/default-avatar.png') }}" alt="">
                                            </span>
                                            <strong>{{ $o->customer->fullname }}</strong>
                                        </a>
                                    </td>
                                    <td>Rp {{ number_format($o->total_price, '0', ',', '.') }}</td>
                                    <td>
                                        <a href="{{ url('admin/shop/edit', [$o->shop_id]) }}">{{ $o->shop->shop_name or '-' }}</a>
                                    </td>
                                    <td>{{ $o->area->area_name or '-' }}</td>
                                    {{--<td>
                                        <a href="{{ url('admin/order/edit', [$o->order_id]) }}" class="btn btn-xs btn-success" 
                                            data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('admin/order', [$o->order_id]) }}" class="btn btn-xs btn-danger"
                                            data-toggle="tooltip" data-placement="top" title="Delete"
                                            data-method="DELETE" data-token="{{csrf_token()}}" 
                                            data-confirm="Are you sure want to delete this data?">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>--}}
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection