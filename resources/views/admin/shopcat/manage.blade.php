@extends('layouts.admin')

@section('breadcrumb')
    <li class='active'>Shop Category</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-2 m-b-md">
            <a href="{{ url('admin/shop-category/create') }}" class="btn btn-primary"><i class="fa fa-add"></i> Create New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Shop Category Name</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($sc as $s)
                                <tr>
                                    <td class="order-id td-hover">
                                        <a href="{{ url('admin/shop-category/edit', [$s->shop_category_id]) }}">
                                            <strong>{{ $s->shop_category_id }}</strong>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/shop-category/edit', [$s->shop_category_id]) }}">
                                            {{ $s->shop_category_name or '-' }}
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection