@extends('layouts.admin')

@section('breadcrumb')
    <li><a href="{{ url('admin/shop-category') }}">Shop Category</a></li>
    <li class='active'>Create</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form method="POST" action="{{url('admin/shop-category')}}" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop Category Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="shop_category_name" value="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Create" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection