@extends('layouts.admin')

@section('breadcrumb')
    <li><a href="{{ url('admin/variable') }}">Variable</a></li>
    <li class='active'>Create</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form method="POST" action="{{url('admin/variable')}}" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Variable Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="variable_name" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Type</label>
                            <div class="col-sm-4">
                                <select name="type" class="form-control" required>
                                    <option value="I">Integer</option>    
                                    <option value="S">String</option>
                                    <option value="F">Float</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Value</label>
                            <div class="col-sm-6">
                                <input type="text" name="value" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Create" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection