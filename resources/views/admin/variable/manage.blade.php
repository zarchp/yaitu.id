@extends('layouts.admin')

@section('breadcrumb')
    <li class='active'>Variable</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-2 m-b-md">
            <a href="{{ url('admin/variable/create') }}" class="btn btn-primary"><i class="fa fa-add"></i> Create New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Variable</th>
                                    <th>Type</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($var as $v)
                                <tr>
                                    <td class="order-id td-hover">
                                        <a href="{{ url('admin/variable/edit', [$v->variable_id]) }}">
                                            <strong>{{ $v->variable_id }}</strong>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/variable/edit', [$v->variable_id]) }}">
                                            {{ $v->variable_name or '-' }}
                                        </a>
                                    </td>
                                    <td data-order="1">
                                        @if($v->type=='I')
                                            Integer
                                        @elseif($v->type=='S')
                                            String
                                        @elseif($v->type=='F')
                                            Float
                                        @elseif($v->type=='D')
                                            Double
                                        @endif
                                    </td>
                                    <td>{{ $v->value }}</td>
                                    <td>
                                        <a href="{{ url('admin/variable/edit', [$v->variable_id]) }}" class="btn btn-xs btn-success" 
                                            data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('admin/variable', [$v->variable_id]) }}" class="btn btn-xs btn-danger"
                                            data-toggle="tooltip" data-placement="top" title="Delete"
                                            data-method="DELETE" data-token="{{csrf_token()}}" 
                                            data-confirm="Are you sure want to delete this data?">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection