@extends('layouts.admin')

@section('breadcrumb')
    <li><a href="{{ url('admin/variable') }}">Variable</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form method="POST" action="{{url('admin/variable',[$var->variable_id])}}" class="form-horizontal">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Variable Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="variable_name" value="{{ $var->variable_name }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Type</label>
                            <div class="col-sm-4">
                                <select name="type" class="form-control" required>
                                    <option value="I" {{ $var->type=='I'?'selected':'' }}>Integer</option>    
                                    <option value="S" {{ $var->type=='S'?'selected':'' }}>String</option>
                                    <option value="F" {{ $var->type=='F'?'selected':'' }}>Float</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Value</label>
                            <div class="col-sm-6">
                                <input type="text" name="value" value="{{ $var->value }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection