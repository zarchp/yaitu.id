@extends('layouts.admin')

@section('breadcrumb')
    <li class='active'>Fee</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-2 m-b-md">
            <a href="{{ url('admin/fee/create') }}" class="btn btn-primary"><i class="fa fa-add"></i> Create New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Shop</th>
                                    <th>Area</th>
                                    <th>Fee</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($fee as $f)
                                <tr>
                                    <td class="order-id td-hover">
                                        <a href="{{ url('admin/fee/edit', [$f->fee_id]) }}">
                                            <strong>{{ $f->fee_id }}</strong>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/fee/edit', [$f->fee_id]) }}">
                                            {{ $f->shop->shop_name or '-' }}
                                        </a>
                                    </td>
                                    <td data-order="1">
                                        {{ $f->area->area_name or '-' }}
                                    </td>
                                    <td>Rp {{ $f->fee_amount }}</td>
                                    <td>
                                        <a href="{{ url('admin/fee/edit', [$f->fee_id]) }}" class="btn btn-xs btn-success" 
                                            data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('admin/fee', [$f->fee_id]) }}" class="btn btn-xs btn-danger"
                                            data-toggle="tooltip" data-placement="top" title="Delete"
                                            data-method="DELETE" data-token="{{csrf_token()}}" 
                                            data-confirm="Are you sure want to delete this data?">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection