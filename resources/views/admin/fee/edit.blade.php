@extends('layouts.admin')

@push('js')
    @include('admin.shared.address')
@endpush

@section('breadcrumb')
    @if($source == 'shop')
        <li><a href="{{ url('admin/shop') }}">Shop</a></li>
        <li><a href="{{ url('admin/shop/edit', $fee->shop_id) }}">{{ $fee->shop->shop_name }}</a></li>
    @endif
    <li><a href="{{ url('admin/fee') }}">Fee</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form method="POST" action="{{url('admin/fee',[$fee->fee_id])}}" class="form-horizontal">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        @if($source == 'shop')
                            <input type="hidden" name="source" class="form-control" value="shop">
                        @endif
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Shop</label>
                            <div class="col-sm-6">
                                <input type="hidden" name="shop_id" class="form-control" value="{{ $fee->shop_id }}">
                                <input type="text" name="shop_name" class="form-control" value="{{ $fee->shop->shop_name }}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Province</label>
                            <div class="col-sm-6">
                                <select name="provinsi_id" class="form-control select2 province" required>
                                    <option value="">-- Select Province --</option>
                                    @foreach($prov as $p)
                                        <option value="{{ $p->provinsi_id }}" {{ $fee->area->kabupaten->provinsi_id==$p->provinsi_id?'selected':'' }}>{{ $p->provinsi_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Regency/City</label>
                            <div class="col-sm-6">
                                <select name="kabupaten_id" class="form-control select2 regency" required>
                                    <option value="">-- Select Regency/City --</option>
                                    @foreach($kab as $k)
                                        <option value="{{ $k->kabupaten_id }}" {{ $fee->area->kabupaten->kabupaten_id==$k->kabupaten_id?'selected':'' }}>{{ $k->kabupaten_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Area</label>
                            <div class="col-sm-6">
                                <select name="area_id" class="form-control select2 area" required>
                                    <option value="">-- Select Area --</option>
                                    @foreach($area as $a)
                                        <option value="{{ $a->area_id }}" {{ $fee->area_id==$a->area_id?'selected':'' }}>{{ $a->area_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Fee</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Rp</span>
                                    <input type="text" name="fee_amount" value="{{ $fee->fee_amount }}" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection