@extends('layouts.admin')

@push('js')
    @include('admin.shared.address')
@endpush

@section('breadcrumb')
    <li><a href="{{ url('admin/area') }}">Area</a></li>
    <li class='active'>Edit</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form method="POST" action="{{url('admin/area',[$area->area_id])}}" class="form-horizontal">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Province</label>
                            <div class="col-sm-6">
                                <select name="provinsi_id" class="form-control select2 province" required>
                                    <option value="">-- Select Province --</option>
                                    @foreach($prov as $p)
                                        <option value="{{ $p->provinsi_id }}" 
                                            {{ $p->provinsi_id==$area->kabupaten->provinsi->provinsi_id?'selected':'' }}>
                                            {{ $p->provinsi_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Regency/City</label>
                            <div class="col-sm-6">
                                <select name="kabupaten_id" class="form-control select2 regency" required>
                                    <option value="">-- Select Regency/City --</option>
                                    @foreach($kab as $k)
                                        <option value="{{ $k->kabupaten_id }}" 
                                            {{ $k->kabupaten_id==$area->kabupaten->kabupaten_id?'selected':'' }}>
                                            {{ $k->kabupaten_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Area Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="area_name" value="{{$area->area_name}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">Area Status</label>
                            <div class="col-sm-4">
                                <select name="is_active" class="form-control">
                                    <option value="0" {{ $area->is_active=='0'?"selected":"" }}>Disabled</option>
                                    <option value="1" {{ $area->is_active=='1'?"selected":"" }}>Active</option>    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1"></label>
                            <div class="col-sm-9">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection