@extends('layouts.admin')

@section('breadcrumb')
    <li class='active'>Area</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-2 m-b-md">
            <a href="{{ url('admin/area/create') }}" class="btn btn-primary"><i class="fa fa-add"></i> Create New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-middle datatable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Area</th>
                                    <th>Regency/City</th>
                                    <th>Province</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($area as $o)
                                <tr>
                                    <td class="order-id td-hover">
                                        <a href="{{ url('admin/area/edit', [$o->area_id]) }}">
                                            <strong>{{ $o->area_id }}</strong>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/area/edit', [$o->area_id]) }}">
                                            {{ $o->area_name or '-' }}
                                        </a>
                                    </td>
                                    <td>{{ $o->kabupaten->kabupaten_name or '-' }}</td>
                                    <td>{{ $o->kabupaten->provinsi->provinsi_name or '-' }}</td>
                                    <td data-order="1">
                                        @if($o->is_active=='1')
                                            <span class="label label-success label-pill">Active</span>
                                        @else
                                            <span class="label label-danger label-pill">Disabled</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/area/edit', [$o->area_id]) }}" class="btn btn-xs btn-success" 
                                            data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('admin/area', [$o->area_id]) }}" class="btn btn-xs btn-danger"
                                            data-toggle="tooltip" data-placement="top" title="Delete"
                                            data-method="DELETE" data-token="{{csrf_token()}}" 
                                            data-confirm="Are you sure want to delete this data?">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection