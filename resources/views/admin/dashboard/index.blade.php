@extends('layouts.admin')

@section('breadcrumb', '')

@section('content')
    <div class="row gutter-xs">
        <div class="col-md-6 col-lg-3">
            <a href="{{ url('admin/user?type=C') }}" class="no-underline">
                <div class="card bg-primary">
                    <div class="card-body">
                        <div class="media">
                            <div class="media-middle media-left">
                                <span class="bg-primary-inverse circle sq-48">
                                    <span class="icon icon-user"></span>
                                </span>
                            </div>
                            <div class="media-middle media-body">
                                <h6 class="media-heading">Customer</h6>
                                <h3 class="media-heading">
                                    <span class="fw-l">{{ $customer }}</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
            <a href="{{ url('admin/user?type=A') }}" class="no-underline">
                <div class="card bg-info">
                    <div class="card-body">
                        <div class="media">
                            <div class="media-middle media-left">
                                <span class="bg-info-inverse circle sq-48">
                                    <span class="icon icon-user-secret"></span>
                                </span>
                            </div>
                            <div class="media-middle media-body">
                                <h6 class="media-heading">Agent</h6>
                                <h3 class="media-heading">
                                    <span class="fw-l">{{ $agent }}</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
            <a href="{{ url('admin/shop') }}" class="no-underline">
                <div class="card bg-success">
                    <div class="card-body">
                        <div class="media">
                            <div class="media-middle media-left">
                                <span class="bg-success-inverse circle sq-48">
                                    <span class="icon fa-cutlery"></span>
                                </span>
                            </div>
                            <div class="media-middle media-body">
                                <h6 class="media-heading">Shop</h6>
                                <h3 class="media-heading">
                                    <span class="fw-l">{{ $shop }}</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card bg-warning">
                <div class="card-body">
                    <div class="media">
                        <div class="media-middle media-left">
                            <span class="bg-warning-inverse circle sq-48">
                                <span class="icon fa-shopping-cart"></span>
                            </span>
                        </div>
                        <div class="media-middle media-body">
                            <h6 class="media-heading">Order</h6>
                            <h3 class="media-heading">
                                <span class="fw-l">{{ $order }}</span>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row gutter-xs">
        <div class="col-md-6">
        <div class="card">
            <div class="card-body">
            <h4 class="card-title">New Customer/Agent ({{ date('F Y') }})</h4>
            </div>
            <div class="card-body">
            <div class="card-chart">
                <canvas id="demo-visitors" data-chart="bar" data-animation="false" 
                data-labels='[{{ $new_user_key }}]' 
                data-values='[{
                        "label": "New Customer/Agent", "backgroundColor": "#2196f3", "borderColor": "#2196f3",  
                        "data": [{{ $new_user }}]
                    }]' 
                    data-hide='["legend", "scalesX"]' height="150">
                </canvas>
            </div>
            </div>
        </div>
        </div>
        <div class="col-md-6">
        <div class="card">
            <div class="card-body">
            <h4 class="card-title">Total Order ({{ date('F Y') }})</h4>
            </div>
            <div class="card-body">
            <div class="card-chart">
                <canvas id="demo-sales" data-chart="bar" data-animation="false" 
                    data-labels='[{{ $total_order_key }}]' 
                    data-values='[{
                            "label": "Sales", "backgroundColor": "#ef6c00", "borderColor": "#ef6c00",  
                            "data": [{{ $total_order }}]
                        }]' 
                        data-hide='["legend", "scalesX"]' height="150">
                </canvas>
            </div>
            </div>
        </div>
        </div>
    </div>
    <div class="row gutter-xs">
        <div class="col-md-6">
        <div class="card">
            <div class="card-header">
            <strong>Traffic Source / Top Referrals</strong>
            </div>
            <div class="card-body">
            <div class="row">
                <div class="col-md-6 m-y">
                <ul class="list-group list-group-divided">
                    <li class="list-group-item">
                    <div class="media">
                        <div class="media-middle media-body">
                        <h6 class="media-heading">
                            <span class="text-muted">Direct</span>
                        </h6>
                        <h4 class="media-heading">67%
                            <small>691,279</small>
                        </h4>
                        </div>
                        <div class="media-middle media-right">
                        <span class="bg-info circle sq-40">
                            <span class="icon icon-arrow-right"></span>
                        </span>
                        </div>
                    </div>
                    </li>
                    <li class="list-group-item">
                    <div class="media">
                        <div class="media-middle media-body">
                        <h6 class="media-heading">
                            <span class="text-muted">Referrals</span>
                        </h6>
                        <h4 class="media-heading">21%
                            <small>216,670</small>
                        </h4>
                        </div>
                        <div class="media-middle media-right">
                        <span class="bg-info circle sq-40">
                            <span class="icon icon-link"></span>
                        </span>
                        </div>
                    </div>
                    </li>
                    <li class="list-group-item">
                    <div class="media">
                        <div class="media-middle media-body">
                        <h6 class="media-heading">
                            <span class="text-muted">Search Engines</span>
                        </h6>
                        <h4 class="media-heading">12%
                            <small>123,811</small>
                        </h4>
                        </div>
                        <div class="media-middle media-right">
                        <span class="bg-info circle sq-40">
                            <span class="icon icon-search"></span>
                        </span>
                        </div>
                    </div>
                    </li>
                </ul>
                </div>
                <div class="col-md-6 m-y">
                <table class="table table-borderless table-fixed">
                    <tbody>
                    <tr>
                        <td class="col-xs-1 text-left">1.</td>
                        <td class="col-xs-7 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">themeforest.net</span>
                        </a>
                        </td>
                        <td class="col-xs-4 text-right">115,928</td>
                    </tr>
                    <tr>
                        <td class="col-xs-1 text-left">2.</td>
                        <td class="col-xs-7 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">facebook.com</span>
                        </a>
                        </td>
                        <td class="col-xs-4 text-right">43,651</td>
                    </tr>
                    <tr>
                        <td class="col-xs-1 text-left">3.</td>
                        <td class="col-xs-7 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">twitter.com</span>
                        </a>
                        </td>
                        <td class="col-xs-4 text-right">20,117</td>
                    </tr>
                    <tr>
                        <td class="col-xs-1 text-left">4.</td>
                        <td class="col-xs-7 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">linkedin.com</span>
                        </a>
                        </td>
                        <td class="col-xs-4 text-right">19,115</td>
                    </tr>
                    <tr>
                        <td class="col-xs-1 text-left">5.</td>
                        <td class="col-xs-7 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">pinterest.com</span>
                        </a>
                        </td>
                        <td class="col-xs-4 text-right">11,292</td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
        </div>
        <div class="col-md-6">
        <div class="card">
            <div class="card-header">
            <strong>Sales / Top Brands</strong>
            </div>
            <div class="card-body">
            <div class="row">
                <div class="col-md-6 m-y">
                <ul class="list-group list-group-divided">
                    <li class="list-group-item">
                    <div class="media">
                        <div class="media-middle media-body">
                        <h6 class="media-heading">
                            <span class="text-muted">Women's Clothing</span>
                        </h6>
                        <h4 class="media-heading">28%
                            <small>$43,498.69</small>
                        </h4>
                        </div>
                        <div class="media-middle media-right">
                        <span class="bg-warning circle sq-40">
                            <span class="icon icon-female"></span>
                        </span>
                        </div>
                    </div>
                    </li>
                    <li class="list-group-item">
                    <div class="media">
                        <div class="media-middle media-body">
                        <h6 class="media-heading">
                            <span class="text-muted">Men's Clothing</span>
                        </h6>
                        <h4 class="media-heading">19%
                            <small>$29,516.97</small>
                        </h4>
                        </div>
                        <div class="media-middle media-right">
                        <span class="bg-warning circle sq-40">
                            <span class="icon icon-male"></span>
                        </span>
                        </div>
                    </div>
                    </li>
                    <li class="list-group-item">
                    <div class="media">
                        <div class="media-middle media-body">
                        <h6 class="media-heading">
                            <span class="text-muted">Computers</span>
                        </h6>
                        <h4 class="media-heading">15%
                            <small>$23,302.87</small>
                        </h4>
                        </div>
                        <div class="media-middle media-right">
                        <span class="bg-warning circle sq-40">
                            <span class="icon icon-laptop"></span>
                        </span>
                        </div>
                    </div>
                    </li>
                </ul>
                </div>
                <div class="col-md-6 m-y">
                <table class="table table-borderless table-fixed">
                    <tbody>
                    <tr>
                        <td class="col-xs-1 text-left">1.</td>
                        <td class="col-xs-6 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">Banana Republic</span>
                        </a>
                        </td>
                        <td class="col-xs-5 text-right">$7,124.23</td>
                    </tr>
                    <tr>
                        <td class="col-xs-1 text-left">2.</td>
                        <td class="col-xs-6 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">Calvin Klein</span>
                        </a>
                        </td>
                        <td class="col-xs-5 text-right">$6,389.26</td>
                    </tr>
                    <tr>
                        <td class="col-xs-1 text-left">3.</td>
                        <td class="col-xs-6 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">Ralph Lauren</span>
                        </a>
                        </td>
                        <td class="col-xs-5 text-right">$5,826.64</td>
                    </tr>
                    <tr>
                        <td class="col-xs-1 text-left">4.</td>
                        <td class="col-xs-6 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">Tommy Hilfiger</span>
                        </a>
                        </td>
                        <td class="col-xs-5 text-right">$5,064.62</td>
                    </tr>
                    <tr>
                        <td class="col-xs-1 text-left">5.</td>
                        <td class="col-xs-6 text-left">
                        <a class="link-muted" href="#">
                            <span class="truncate">Lenovo</span>
                        </a>
                        </td>
                        <td class="col-xs-5 text-right">$4,536.72</td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection