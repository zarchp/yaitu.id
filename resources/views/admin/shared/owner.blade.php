<script>
    $(function(){
        $('.owner').change(function(){
            var id = $(this).val(),
                shop_id = $('.shop_id').val();
            
            if(shop_id==undefined)
                shop_id = null;

            $.get("{{ url('admin/ajax/owner/') }}/"+id+"/"+shop_id, function( result ) {
                if(result==1){
                    $('.is_owner').attr('disabled', 'disabled');
                    $('.is_owner').val('0');
                    $('.hdn_owner').removeAttr('disabled');
                    $('.hdn_owner').val('0');
                }
                else{
                    $('.is_owner').removeAttr('disabled');
                    $('.hdn_owner').attr('disabled', 'disabled');
                }
            });    
        });
    });
</script>