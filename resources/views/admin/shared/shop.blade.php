<script>
    $(function(){
        $('select[name="shop_type_id"]').change(function(){
            var id = $(this).val();
            if(id==1){
                $('select[name="shop_category_id"]').prop('disabled', false);
            }
            else{
                $('select[name="shop_category_id"]').prop('disabled', true);
            }
        });
    });
</script>