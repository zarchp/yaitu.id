<script>
    $(function () {
        /*$('.country').change(function(){
            var id = $(this).val(),
                options = "<option value=''>-- Select Province --</option>";
            if(id == 'ID' ){
                $('.province').removeAttr('disabled');
            } else {
                $('.province').val('');
                $('.province').attr('disabled', 'disabled');
                $('.regency').val('');
                $('.regency').attr('disabled', 'disabled');
                $('.subdistrict').val('');
                $('.subdistrict').attr('disabled', 'disabled');
            } 
        });*/
        
        $('.province').change(function(){
            var id = $(this).val(),
                options = "<option value=''>-- Select Regency --</option>",
                options2 = "<option value=''>-- Select Subdistrict --</option>",
                options3 = "<option value=''>-- Select Village --</option>";
            $.get("{{ url('admin/ajax/regency') }}/"+id, function( result ) {
                $.each(JSON.parse(result), function(index, data){
                    options += "<option value='"+data.kabupaten_id+"'>"+data.kabupaten_name+"</option>"
                });
                $('.regency').html(options);
                $('.subdistrict').html(options2);
                $('.village').html(options3);
                if(id){
                    $('.regency').removeAttr('disabled');
                } else {
                    $('.regency').attr('disabled', 'disabled');
                }
                $('.subdistrict').val('');
                $('.subdistrict').attr('disabled', 'disabled');
                $('.village').val('');
                $('.village').attr('disabled', 'disabled');
                $('.area').val('');
                $('.area').attr('disabled', 'disabled');
            });    
        });

        $('.regency').change(function(){
            var id = $(this).val(),
                options = "<option value=''>-- Select Subdistrict --</option>",
                options2 = "<option value=''>-- Select Area --</option>";
            $.get("{{ url('admin/ajax/subdistrict') }}/"+id, function( result ) {
                $.each(JSON.parse(result), function(index, data){
                    options += "<option value='"+data.kecamatan_id+"'>"+data.kecamatan_name+"</option>"
                });
                $('.subdistrict').html(options);
                if(id){
                    $('.subdistrict').removeAttr('disabled');
                } else {
                    $('.subdistrict').attr('disabled', 'disabled');
                }
                $('.village').val('');
                $('.village').attr('disabled', 'disabled');
            });

            $.get("{{ url('admin/ajax/area') }}/"+id, function( result ) {
                $.each(JSON.parse(result), function(index, data){
                    options2 += "<option value='"+data.area_id+"'>"+data.area_name+"</option>"
                });
                $('.area').html(options2);
                if(id){
                    $('.area').removeAttr('disabled');
                } else {
                    $('.area').attr('disabled', 'disabled');
                }
            });
        });

        $('.subdistrict').change(function(){
            var id = $(this).val(),
                options = "<option value=''>-- Select Village --</option>";
            $.get("{{ url('admin/ajax/village/') }}/"+id, function( result ) {
                $.each(JSON.parse(result), function(index, data){
                    options += "<option value='"+data.kelurahan_id+"'>"+data.kelurahan_name+"</option>"
                });
                $('.village').html(options);
                if(id){
                    $('.village').removeAttr('disabled');
                } else {
                    $('.village').attr('disabled', 'disabled');
                }
            });    
        });
    });
</script>