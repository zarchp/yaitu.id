<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('.delete').click(function(e) {
            var d = confirm('Are you sure want to delete this data?');
            if (d != true)
                e.preventDefault();
        });

        $('.dropdown-status li a').click(function(){
            var text = $(this).text(),
                val = $(this).attr('value'),
                btnStatus = $(this).parent().parent().parent().find(".btn-status"),
                txtStatus = $(this).parent().parent().parent().find(".status-text"),
                orderId = $(this).parent().parent().parent().parent().find(".order-id a strong").text();

            if (orderId == '') {
                orderId = $(this).parent().parent().parent().parent().parent().find(".order-id").text();
            }

            $.ajax({
                url: "/admin/order/"+orderId+"/status",
                data: {status: val},
                type: 'PUT',
                success: function(jqXHR, textStatus) {
                    txtStatus.text(text);
                    btnStatus.removeClass("btn-primary btn-success btn-info btn-warning btn-danger");
                    if(val=='C')
                        btnStatus.addClass("btn-success");
                    else if(val=='L')
                        btnStatus.addClass("btn-danger");
                    else if(val=='N')
                        btnStatus.addClass("btn-warning");
                    else if(val=='T' || val=='O' || val=='D')
                        btnStatus.addClass("btn-info"); 
                    
                    location.reload();
                },
                fail: function(jqXHR, textStatus) {
                    alert( "Request failed: " + textStatus );
                }
            });
        });
    });

    function getBaseUrl() {
        return window.location.href.match(/^.*\//);
    }
</script>