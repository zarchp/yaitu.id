<script>
    $(function () {
        $('.shop').change(function(){
            var id = $(this).val(),
                link = $('.link').val(),
                options = "<option value=''>-- Select Menu Category --</option>";

            if(link=='C'){
                $.get("{{ url('admin/ajax/shopmenucat/') }}/"+id, function( result ) {
                    $.each(JSON.parse(result), function(index, data){
                        options += "<option value='"+data.shop_menu_category_id+"'>"+data.category_name+"</option>"
                    });
                    $('.shopmenucat').html(options);
                    if(id){
                        $('.shopmenucat').removeAttr('disabled');
                    } else {
                        $('.shopmenucat').attr('disabled', 'disabled');
                    }
                });
            }
        });

        $('.link').change(function(){
            var link = $(this).val(),
                options = "<option value=''>-- Select Menu Category --</option>";

            if(link!='C'){
                $('.shopmenucat').html(options);
                $('.shopmenucat').attr('disabled', 'disabled');
            }
        });
    });
</script>