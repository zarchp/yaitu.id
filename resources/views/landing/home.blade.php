<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" href="assets/images/favicon.png" type="image/png" sizes="16x16">
    <meta name="description" content="Yaitu melayani kebuhutahnmu layanan tolong menolong di Indonesia">
    <meta name="keywords" content="On demand service, layanan pesan antar, delivery service, saling menolong">
    <link href="{{ asset('landing/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans%7CRoboto:300,400,500%7CMontserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/animate.css') }}"> <!-- Resource style -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/ionicons.min.css') }}"> <!-- Resource style -->
    <link href="{{ asset('landing/assets/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div class="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#main"><img src="{{ asset('landing/assets/icons/pi-logo.png') }}" width="40" alt="Logo"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav nav-white">
                        <li><a class="page-scroll" href="#main">Home</a></li>
                        <li><a class="page-scroll" href="#services">Services</a></li>
                        <li><a class="page-scroll" href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav><!-- /.navbar-collapse -->

        <div id="main" class="main">
            <div class="hero-1">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="intro">
                                <h1 class="wow fadeInDown" data-wow-delay="0.1s">Mari Berbagi Dan Saling Membantu</h1>
                                <p class="wow fadeInDown" data-wow-delay="0.2s">Yang akan menolong kita, adalah orang-orang terdekat di sekitar kita. Mari berbagi dan saling membantu dengan saudara-saudara yang ada disekeliling kita.</p>
                                <a href="#" class="btn btn-action wow fadeInDown" data-wow-delay="0.3s">Know More</a>
                                <a href="#" class="btn btn-action wow fadeInDown" data-wow-delay="0.3s">See Features</a>
                                <p class="wow fadeInDown" data-wow-delay="0.3s">
                                    <a href="https://play.google.com/store/apps/details?id=id.yaitu.android"><img src="{{ asset('landing/assets/images/google-play-badge.png') }}" width="200px"></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Pi-Hero -->


            <div id="services" class="features-boxed">
                <div class="container">
                    <div class="row">
                        <div class="boxed-intro wow fadeInDown">
                            <div class="col-sm-6">
                                <h4>Our Services</h4>
                                <h1>The Exellence we deliver</h1>
                                <p>Kami memberikan layanan jasa terbaik untuk Anda, yang memungkinkan Anda sebanyak mungkin menghemat waktu dan tenaga.
                                Kami memberikan orang-orang terdekat Anda yang dengan setia akan menolong Anda</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 wow fadeInDown">
                            <div class="box-inner">
                                <div class="box-icon">
                                    <img src="{{ asset('landing/assets/icons/1.png') }}" width="45" alt="Feature">
                                </div>
                                <div class="box-info">
                                    <h1>Layanan Pembelian</h1>
                                    <p>Kami menolong Anda yang ingin mempergunakan layanan pembelian produk di toko-toko sekitar Anda</p>
                                </div>
                                <div class="box-arrow">
                                    <a href="#"><i class="ion-ios-arrow-thin-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cta-big">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-7">
                            <div class="cta-big-inner wow fadeInRight">
                                <h1>Bergabunglah dengan orang-orang disekeliling Anda.</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="contact" class="contact">
                <div class="container">
                    <div class="col-sm-6 col-sm-offset-3 text-center">
                        <div class="contact-intro wow fadeInDown">
                            <h1>Ask us anything and we'll get back soon in a day</h1>
                            <p>
                                Silahkan hubungi kami untuk informasi lebih lengkap.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="contact-form wow fadeInDown">
                            <form id="contact-form" method="post" action="#">
                                <div class="messages"></div>
                                <div class="controls">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_name">Name*</label>
                                                <input id="form_name" type="text" name="name" class="form-control" required="required" data-error="Firstname is required.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_email">Email *</label>
                                                <input id="form_email" type="email" name="email" class="form-control" required="required" data-error="Valid email is required.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="form_message">Message *</label>
                                                <textarea id="form_message" name="message" class="form-control" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="submit" class="btn btn-success btn-send" value="Send message">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="footer-lg wow fadeIn">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="footer-left">
                                <div class="footer-img">
                                    <img src="{{ asset('landing/assets/icons/pi-logo.png') }}" width="50" alt="Logo">
                                </div>
                                <p>Yaitu | Melayani Kebutuhanmu. Platform tolong menolong dan berbagi antar kita dan orang-orang di sekeliling kita. Anda dapat 
                                    mempergunakan aplikasi Yaitu sebagai wujud kepedulian Anda terhadap sesama. </p>
                                <p>&copy; 2017 Yaitu | Melayani Kebutuhanmu</p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="footer-right">
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Product Details</a></li>
                                    <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Scroll To Top -->

            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-up"></i>
            </a>

        <!-- Scroll To Top Ends-->

        </div> <!-- Main -->
    </div><!-- Wrapper -->

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('landing/assets/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/validator.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/contact.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/custom.js') }}"></script>
    <script>
        $(function(){

        });
    </script>
</body>
</html>
