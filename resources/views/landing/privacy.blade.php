<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" href="assets/images/favicon.png" type="image/png" sizes="16x16">
    <meta name="description" content="Yaitu melayani kebuhutahnmu layanan tolong menolong di Indonesia">
    <meta name="keywords" content="On demand service, layanan pesan antar, delivery service, saling menolong">
    <link href="{{ asset('landing/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans%7CRoboto:300,400,500%7CMontserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/animate.css') }}"> <!-- Resource style -->
    <link rel="stylesheet" href="{{ asset('landing/assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/assets/css/ionicons.min.css') }}"> <!-- Resource style -->
    <link href="{{ asset('landing/assets/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div class="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#main"><img src="{{ asset('landing/assets/icons/pi-logo.png') }}" width="40" alt="Logo"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav nav-white">
                        <li><a class="page-scroll" href="{{ url('/#main') }}">Home</a></li>
                        <li><a class="page-scroll" href="{{ url('/#services') }}">Services</a></li>
                        <li><a class="page-scroll" href="{{ url('/#contact') }}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav><!-- /.navbar-collapse -->

        <div id="main" class="main">
            {{--  <div class="hero-1">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="intro">
                                <h1 class="wow fadeInDown" data-wow-delay="0.1s">Mari Berbagi Dan Saling Membantu</h1>
                                <p class="wow fadeInDown" data-wow-delay="0.2s">Yang akan menolong kita, adalah orang-orang terdekat di sekitar kita. Mari berbagi dan saling membantu dengan saudara-saudara yang ada disekeliling kita.</p>
                                <a href="#" class="btn btn-action wow fadeInDown" data-wow-delay="0.3s">Know More</a>
                                <a href="#" class="btn btn-action wow fadeInDown" data-wow-delay="0.3s">See Features</a>
                                <p class="wow fadeInDown" data-wow-delay="0.3s">
                                    <a href="https://play.google.com/store/apps/details?id=id.yaitu.android"><img src="{{ asset('landing/assets/images/google-play-badge.png') }}" width="200px"></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  --}}

            <div class="cta-big" style="
                background:linear-gradient(to right, rgba(0, 0, 0, 1), rgba(0, 0, 0, 0.8)), url({{ asset('landing/assets/images/bg.jpg') }}) no-repeat center center; 
                padding:120px 0 40px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="cta-big-inner wow fadeInRight">
                                <h1>Privacy Policy</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="privacy" class="privacy">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                                Kebijakan Privasi berikut ini
                                menjelaskan bagaimana kami mengumpulkan, menggunakan, memindahkan,
                                mengungkapkan dan melindungi informasi pribadi anda yang dapat diidentifikasi
                                yang diperoleh melalui Aplikasi kami (sebagaimana didefinisikan di bawah). 
                            </p>
                            <p>
                                Mohon anda membaca Kebijakan Privasi ini dengan seksama untuk memastikan bahwa anda
                                memahami bagaimana ketentuan Kebijakan Privasi ini kami berlakukan. Kebijakan
                                Privasi ini disertakan sebagai bagian dari Ketentuan Penggunaan kami. Kebijakan
                                Privasi ini mencakup hal-hal sebagai berikut :
                            </p>
                            <ol>
                                <li>
                                    <span>Definisi</span>
                                </li>
                                <li>
                                    <span>Informasi yang kami kumpulkan</span>
                                </li>
                                <li>
                                    <span>Penggunaan informasi yang kami kumpulkan</span>
                                </li>
                                <li>
                                    <span>Pemberian informasi yang kami kumpulkan</span>
                                </li>
                                <li>
                                    <span>Penahanan informasi yang kami kumpulkan</span>
                                </li>
                                <li>
                                    <span>Keamanan</span>
                                </li>
                                <li>
                                    <span>Perubahan atas Kebijakan Privasi ini</span>
                                </li>
                                <li>
                                    <span>Lain-lain</span>
                                </li>
                                <li>
                                    <span>Pengakuan dan persetujuan</span>
                                </li>
                                <li>
                                    <span>Berhenti menerima e-mail</span>
                                </li>
                                <li>
                                    <span>Cara untuk menghubungi kami</span>
                                </li>
                            </ol>

                            <p style="margin-top:40px;">
                                <strong>
                                    Penggunaan anda atas aplikasi dan layanan kami tunduk pada Ketentuan Penggunaan dan Kebijakan Privasi 
                                    ini dan mengindikasikan persetujuan anda terhadap Ketentuan Penggunaan dan Kebijakan Privasi tersebut.
                                </strong>
                            </p>
                            <ul class="first">
                                <li><span>Definisi</span></b></li>
                                <ul>
                                    <li>
                                        <span>
                                            "Kami" berarti PT Melayani Kebutuhan Indonesia, suatu
                                            perusahaan yang didirikan berdasarkan hukum Negara Republik Indonesia.
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                        "Aplikasi" berarti suatu aplikasi piranti lunak yang
                                        telah kami kembangkan yang merupakan suatu sarana untuk menemukan Layanan
                                        yang disediakan oleh pihak ketiga.</span>
                                    </li>
                                    <li>
                                        <span>
                                            "Penyedia Layanan" berarti suatu pihak ketiga yang
                                            menyediakan Layanan melalui Aplikasi.
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            "Layanan" berarti layanan yang ditawarkan oleh
                                            Penyedia Layanan melalui Aplikasi yang dapat mencakup (i) layanan kurir
                                            instan, (ii) transportasi, (iii) pengiriman makanan dan (iv) pembelanjaan
                                            pribadi.
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            "Informasi Pribadi" berarti informasi mengenai anda
                                            yang secara pribadi dapat diidentifikasi yang dikumpulkan melalui
                                            Aplikasi, seperti nama, alamat, tanggal lahir,
                                            pekerjaan, nomor telepon, alamat surat elektronik (e-mail) anda dan/atau
                                            sejenisnya, dan informasi lain yang mungkin dapat mengidentifikasi orang
                                            tertentu yang sedang menggunakan Aplikasi.
                                        </span>
                                    </li>
                                    <li><span>"Situs Web" berarti situs web kami di <a href="{{ url('/') }}">www.yaitu.id</a>.</span></li>
                                </ul>
                                <li><span>Informasi yang Kami Kumpulkan</span></li>
                                <ul>
                                    <li><span>Kami mengumpulkan Informasi Pribadi tertentu dari anda agar Aplikasi dapat menemukan Layanan dari Penyedia Layanan. Anda akan langsung memberikan Informasi Pribadi (sebagai contoh, saat anda mendaftar) dan beberapa informasi akan secara otomatis dikumpulkan ketika anda menggunakan Aplikasi.</span></li>
                                    <li><span>Ketika anda mengunjungi Situs web kami, administrator situs web kami akan memproses data teknis seperti alamat IP anda, halaman web yang pernah anda kunjungi, browser internet yang anda gunakan, halaman web yang sebelumnya /selanjutnya anda kunjungi dan durasi setiap kunjungan/sesi yang memungkinkan kami untuk mengirimkan fungsi-fungsi Situs web. Sebagai tambahan, dalam beberapa hal, browser dapat menyarankan anda agar geo-location anda memungkinkan kami untuk memberikan anda suatu pengalaman yang lebih baik. Dengan data teknis ini administrator-administrator Situs web kami dapat mengelola Situs web, misalnya dengan menyelesaikan kesulitan-kesulitan teknis atau memperbaiki kemampuan untuk dapat diaksesnya bagian-baigan tertentu dari Situs web. Dengan cara ini, kami dapat memastikan bahwa Anda dapat (terus) menemukan informasi pada Situs web dengan cara yang cepat dan sederhana.</span></li>
                                    <li><span>Informasi yang Anda berikan secara langsung.</span></li>
                                    <li><span>Pada saat mendaftar pada Aplikasi, anda akan memberikan kepada kami alamat surat elektronik / surel (email), nama, nomor telepon, dan sandi akun anda.</span></li>
                                    <li><span>Ketika anda menggunakan Aplikasi untuk menemukan suatu layanan, anda memberikan informasi pada kami, yaitu lokasi dan tujuan anda. Anda juga memberikan informasi kepada kami mengenai barang yang anda kirim/antar dan/atau beli dan biaya pembelanjaan anda ketika anda menggunakan layanan kurir instan atau pembelanjaan pribadi. Ketika anda menggunakan Aplikasi kami, kami juga akan memproses data teknis anda seperti alamat IP, Identitas (ID) Perangkat atau alamat MAC, dan informasi mengenai pabrikan, model, dan sistem operasi dari perangkat bergerak/mobile device anda. Kami menggunakan data ini untuk memungkinkan kami untuk mengirimkan fungsi-fungsi dari Aplikasi, menyelesaikan kesulitan-kesulitan teknis, menyediakan bagi anda versi Aplikasi yang benar dan terkini dan untuk meningkatkan fungsi Aplikasi.</span></li>
                                    <li><span>Kami akan meminta nomor telepon seseorang yang dapat dihubungi oleh Penyedia Layanan untuk melengkapi pesanan anda ketika anda menggunakan Aplikasi untuk menemukan suatu layanan kurir instan. Anda harus mendapatkan persetujuan terlebih dahulu dari orang yang nomor teleponnya anda berikan pada kami untuk memberikan nomor teleponnya pada kami dan untuk kami memberikan nomor telepon tersebut kepada Penyedia Layanan.</span></li>
                                    <li><span>Ketika Anda mengisi ulang (TOP UP) Deposit anda, kami akan mengumpulkan informasi seperti nama bank dimana rekening anda dibuka, nama pemegang rekening, dan jumlah yang anda transfer untuk pengisian ulang (TOP UP).</span></li>
                                    <li><span>Anda dapat memberikan kode rujukan (referral code) kepada teman anda melalui Aplikasi, dimana, kami hanya akan menyiapkan suatu pesan untuk anda kirimkan atau anda terbitkan melalui penyedia media sosial atau surel anda. Anda dapat mengubah pesan yang telah kami siapkan sebelum anda mengirimkannya. Kami tidak akan mengumpulkan data teman anda.</span></li>
                                    <li><span>Informasi yang kami kumpulkan ketika anda menggunakan Aplikasi.</span></li>
                                    <li><span>Ketika anda menggunakan Aplikasi melalui perangkat bergerak / mobile device anda, kami akan melacak dan mengumpulkan informasi geo-location secara real-time. Kami menggunakan informasi ini untuk memungkinkan anda untuk melihat Penyedia Layanan yang berada di daerah anda yang dekat dengan lokasi anda, mengatur lokasi penjemputan dan mengirimkan informasinya kepada Penyedia Layanan yang diminta, dan untuk melihat Penyedia Layanan yang diminta mendekat di suatu peta secara real-time. Kami juga dapat menggunakan informasi geo-location secara real-time ini untuk memberikan bantuan, menyelesaikan kesulitan-kesulitan teknis atau usaha yang mungkin timbul pada saat anda menggunakan Aplikasi. Anda dapat mematikan informasi pelacak geo-location pada tingkatan perangkat untuk sementara waktu. Perangkat bergerak/mobile anda akan memberitahukan anda ketika geo-location anda dilacak dengan menampilkan simbol panah GPS.</span></li>
                                    <li><span>Kami juga melacak dan mengumpulkan informasi geo-location Penyedia Layanan. Ini berarti bahwa kami juga mengumpulkan informasi ketika anda bepergian dengan Penyedia Layanan. Kami juga akan menggunakan informasi geo-location Penyedia Layanan dalam bentuk tanpa nama dan keseluruhan untuk mendapatkan informasi statistic dan informasi pengelolaan dan untuk menyediakan untuk anda fungsi Aplikasi yang ditingkatkan.</span></li>
                                </ul>
                                <li><span>Penggunaan Informasi yang Kami Kumpulkan</span></li>
                                <ul>
                                    <li><span>Kami menggunakan surel, nama, nomor telepon, dan sandi akun anda untuk memverifikasi kepemilikan anda atas suatu akun, untuk berkomunikasi dengan anda sehubungan dengan pesanan anda dan untuk memberikan anda informasi mengenai Aplikasi. Kami juga dapat menggunakan nama, surel, dan nomor telepon anda untuk mengirimkan pesan, pembaharuan yang bersifat umum atas Aplikasi, penawaran-penawaran khusus atau promosi-promosi. Kami juga akan mengirimkan surel kepada anda yang meminta anda untuk berlangganan Daftar Surat Menyurat (Mailing List) kami. Anda dapat setiap saat memilih untuk tidak menerima informasi mengenai pembahrauan ini.</span></li>
                                    <li><span>Kami menggunakan geo-location dan tujuan anda untuk menemukan Penyedia Layanan yang berada di sekitar anda, untuk membantu Penyedia Layanan untuk memperhitungkan biaya dan untuk menganalisa pola penggunaan Aplikasi untuk meningkatkan kinerja Aplikasi.</span></li>
                                    <li><span>Kami menggunakan informasi seperti barang-barang yang anda kirimkan/diantarkan dan/atau beli dan biaya pembelanjaan anda untuk menentukan apakah Aplikasi dapat menerima pesanan anda berdasarkan Ketentuan Penggunaan.</span></li>
                                    <li><span>Kami menggunakan informasi seperti nama bank dimana rekening anda dibuat, nama dimana rekening tersebut disimpan dan jumlah yang ditransfer untuk isi ulang (TOP UP) untuk memastikan pembayaran yang anda lakukan untuk deposit.</span></li>
                                    <li><span>Kami menggunakan Informasi Pribadi dalam bentuk tanpa nama dan secara keseluruhan untuk memantau lebih dekat fitur-fitur mana dari Layanan yang paling sering digunakan, untuk menganalisa pola penggunaan dan untuk menentukan apakah kami akan menawarkan atau fokus pada Layanan kami. Anda dengan ini setuju bahwa data anda akan digunakan oleh pemrosesan data internal kami untuk memberikan Layanan yang lebih baik kepada anda.</span></li>
                                </ul>        
                                <li><span>Pemberian Informasi yang Kami Kumpulkan</span></li>
                                <ul>
                                    <li><span>Setelah menerima pesanan anda, kami akan memberikan informasi seperti nama, nomor telepon, lokasi, tujuan, geo-location, barang yang akan dikirimkan/diantar atau dibeli dan/atau biaya pembelanjaan anda kepada Penyedia Layanan yang menerima permintaan anda atas Layanan. Informasi ini dibutuhkan oleh Penyedia Layanan untuk menghubungi anda, dan/atau menemukan anda dan/atau memenuhi pesanan anda.</span></li>
                                    <li><span>Anda dengan ini setuju dan memberikan wewenang pada kami untuk memberikan Informasi Pribadi anda kepada Penyedia Layanan sebagai suatu bagian dari ketentuan Layanan. Walaupun informasi pribadi anda secara otomatis akan dihapus dari perangkat bergerak milik Penyedia Layanan setelah anda menggunakan Layanan, terdapat kemungkinan dimana Penyedia Layanan dapat menyimpan data anda di perangkat mereka dengan cara apapun. Kami tidak bertanggung jawab atas penyimpanan data dengan cara tersebut dan anda setuju untuk membela, memberikan ganti rugi dan membebaskan kami dan kami tidak akan bertanggung jawab atas segala penyalahgunaan Informasi Pribadi anda oleh Penyedia Layanan setelah berakhirnya Layanan yang diberikan.</span></li>
                                    <li><span>Kami juga akan memberikan nomor telepon dari pihak yang dapat dihubungi yang telah anda berikan kepada kami kepada Penyedia Layanan ketika anda menggunakan Aplikasi untuk menemukan layanan kurir instan.</span></li>
                                    <li><span>Kami dapat mempekerjakan perusahaan-perusahaan dan orang perorangan pihak ketiga untuk memfasilitasi atau memberikan Aplikasi dan layanan-layanan tertentu atas nama kami, untuk memberikan bantuan konsumen, memberikan informasi geo-location kepada Penyedia Layanan kami, untuk melaksanakan layanan-layanan terkait dengan Situs web (misalnya tanpa pembatasan, layanan pemeliharaan, pengelolaan database, analisis web dan penyempurnaan fitur-fitur Situs web) atau untuk membantu kami dalam menganalisa bagaimana Layanan kami digunakan atau untuk penasihat profesional dan auditor eksternal kami, termasuk penasihat hukum, penasihat keuangan, dan konsultan-konsultan. Para pihak ketiga ini hanya memiliki akses atas informasi pribadi anda untuk melakukan tugas-tugas tersebut atas nama kami dan secara kontraktual terikat untuk tidak mengungkapkan atau menggunakan informasi pribadi tersebut untuk tujuan lain apapun.</span></li>
                                    <li><span>Kami tidak membagikan Informasi Pribadi anda kepada pihak manapun selain kepada Penyedia Layanan terkait dan perusahaan dan individu pihak ketiga yang disebutkan di bagian 4.4 di atas, tanpa persetujuan dari anda. Namun demikian, kami akan mengungkapkan Informasi Pribadi anda sepanjang dimintakan secara hukum, atau diperlukan untuk tunduk pada ketentuan perundang-undangan, peraturan-peraturan dan pemerintah, atau dalam hal terjadi sengketa, atau segala bentuk proses hukum antara anda dan kami, atau antara anda dan pengguna lain sehubungan dengan, atau terkait dengan Layanan, atau dalam keadaan darurat yang berkaitan dengan kesehatan dan/atau keselamatan anda.</span></li>
                                    <li><span>Informasi Pribadi anda dapat dialihkan, disimpan, digunakan, dan diproses di suatu yurisdiksi selain Indonesia dimana server-server kami berada. Anda memahami dan setuju atas pengalihan Informasi Pribadi anda ke luar Indonesia.</span></li>
                                    <li><span>Kami tidak menjual atau menyewakan Informasi Pribadi anda kepada pihak ketiga.</span></li>
                                </ul>
                                <li><span>Penahanan Informasi yang Kami Kumpulkan</span></li>
                                <ul>
                                    <li><span>Kami akan menahan informasi anda hingga anda menghapus akun anda pada Aplikasi.</span></li>
                                </ul>
                                <li><span>Keamanan</span></li>
                                <ul>
                                    <li><span>Kami tidak menjamin keamanan database kami dan kami juga tidak menjamin bahwa data yang anda berikan tidak akan ditahan/terganggu ketika sedang dikirimkan kepada kami. Setiap pengiriman informasi oleh anda kepada kami merupakan risiko anda sendiri. Anda tidak boleh mengungkapkan sandi anda kepada siapapun. Bagaimanapun efektifnya suatu teknologi, tidak ada sistem keamanan yang tidak dapat ditembus.</span></li>
                                </ul>
                                <li><span>Perubahan atas Kebijakan Privasi ini</span></li>
                                <ul>
                                    <li><span>Kami dapat mengubah Kebijakan Privasi ini untuk mencerminkan perubahan dalam kegiatan kami. Jika kami mengubah Kebijakan Privasi ini, kami akan memberitahu anda melalui email atau dengan cara pemberitahuan di Situs web 1 hari sebelum perubahan berlaku. Kami menghimbau anda untuk meninjau halaman ini secara berkala untuk mengetahui informasi terbaru tentang bagaimana ketentuan Kebijakan Privasi ini kami berlakukan.</span></li>
                                </ul>
                                <li><span>Lain-lain</span></li>
                                <ul>
                                    <li><span>Bahasa. Kebijakan Privasi ini dibuat dalam bahasa Indonesia, akan mengikat anda dan kami.</span></li>
                                    <li><span>Hukum yang mengatur dan yurisdiksi. Kebijakan Privasi ini diatur oleh dan untuk ditafsirkan dalam hukum Negara Republik Indonesia. Setiap dan seluruh sengketa yang timbul dari kebijakan privasi ini akan diatur oleh yurisdiksi eksklusif dari Pengadilan Negeri Jakarta Selatan.</span></li>
                                </ul>
                                <li><span>Pengakuan dan Persetujuan</span></li>
                                <ul>
                                    <li><span>Dengan menggunakan Aplikasi, anda mengakui bahwa anda telah membaca dan memahami Kebijakan Privasi ini dan Ketentuan Penggunaan dan setuju dan sepakat terhadap penggunaan, praktek, pemrosesan dan pengalihan informasi pribadi anda oleh kami sebagaimana dinyatakan di dalam Kebijakan Privasi ini.</span></li>
                                    <li><span>Anda juga menyatakan bahwa anda memiliki hak untuk membagikan seluruh informasi yang telah anda berikan kepada kami dan untuk memberikan hak kepada kami untuk menggunakan dan membagikan informasi tersebut kepada Penyedia Layanan.</span></li>
                                </ul>
                                <li><span>Berhenti menerima e-mail</span></li>
                                <ul>
                                    <li><span>Kami memiliki kebijakan untuk memilih masuk/keluar dari database. Jika Anda ingin berhenti menerima email dari kami, silahkan klik link unsubscribe yang disertakan pada masing-masing e-mail.</span></li>
                                </ul>
                                <li><span>Cara untuk Menghubungi Kami</span></li>
                                <ul>
                                    <li><span>Jika Anda memiliki pertanyaan lebih lanjut tentang privasi dan keamanan informasi Anda dan ingin memperbarui atau menghapus data Anda maka silakan hubungi kami di: info@yaitu.id atau telepon ke: (021) 730-5184</span></li>
                                </ul>
                            </ul>
                            <p>&nbsp;</p>
                            <p>
                                <strong>Yaitu</strong>
                            </p>
                            <p>
                                Griya Kencana 2 Blok DD No. 31 Sudimara Barat Kecamatan Ciledug Kota Tangerang Banten 15151
                            </p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="footer-lg wow fadeIn">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="footer-left">
                                <div class="footer-img">
                                    <img src="{{ asset('landing/assets/icons/pi-logo.png') }}" width="50" alt="Logo">
                                </div>
                                <p>Yaitu | Melayani Kebutuhanmu. Platform tolong menolong dan berbagi antar kita dan orang-orang di sekeliling kita. Anda dapat 
                                    mempergunakan aplikasi Yaitu sebagai wujud kepedulian Anda terhadap sesama. </p>
                                <p>&copy; 2017 Yaitu | Melayani Kebutuhanmu</p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="footer-right">
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Product Details</a></li>
                                    <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Scroll To Top -->

            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-up"></i>
            </a>

        <!-- Scroll To Top Ends-->

        </div> <!-- Main -->
    </div><!-- Wrapper -->

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('landing/assets/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/validator.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/contact.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing/assets/js/custom.js') }}"></script>
</body>
</html>
