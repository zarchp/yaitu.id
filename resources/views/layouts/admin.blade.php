<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        
        <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
        <link rel="icon" type="image/ico" href="{{ asset('favicon-32x32.ico') }}" sizes="32x32">
        <link rel="icon" type="image/ico" href="{{ asset('favicon-16x16.ico') }}" sizes="16x16">
        <link rel="manifest" href="manifest.json">
        <link rel="mask-icon" href="safari-pinned-tab.svg" color="#0288d1">
        <meta name="theme-color" content="#ffffff">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
        <link rel="stylesheet" href="{{ asset('/css/vendor.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/elephant.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/application.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">
        <!--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">-->
        <link rel="stylesheet" href="{{ asset('/plugins/font-awesome/css/font-awesome.min.css') }}">
        @stack('css')
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!}
        </script>
    </head>
    <body class="layout layout-header-fixed">
        <div class="layout-header">
            <div class="navbar navbar-default">
                <div class="navbar-header">
                    <a class="navbar-brand navbar-brand-center" href="{{ url('admin/dashboard') }}">
                        <img class="navbar-brand-logo" src="{{ asset('img/logo-inverse-128x128.png') }}" alt="Yaitu">
                    </a>
                    <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="bars">
                            <span class="bar-line bar-line-1 out"></span>
                            <span class="bar-line bar-line-2 out"></span>
                            <span class="bar-line bar-line-3 out"></span>
                        </span>
                        <span class="bars bars-x">
                            <span class="bar-line bar-line-4"></span>
                            <span class="bar-line bar-line-5"></span>
                        </span>
                    </button>
                    <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="arrow-up"></span>
                        <span class="ellipsis ellipsis-vertical">
                            {{--  <img class="ellipsis-object" width="32" height="32" src="img/0180441436.jpg" alt="Teddy Wilson">  --}}
                        </span>
                    </button>
                </div>
                <div class="navbar-toggleable">
                <nav id="navbar" class="navbar-collapse collapse">
                    <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="bars">
                            <span class="bar-line bar-line-1 out"></span>
                            <span class="bar-line bar-line-2 out"></span>
                            <span class="bar-line bar-line-3 out"></span>
                            <span class="bar-line bar-line-4 in"></span>
                            <span class="bar-line bar-line-5 in"></span>
                            <span class="bar-line bar-line-6 in"></span>
                        </span>
                    </button>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="visible-xs-block">
                            <h4 class="navbar-text text-center">Hi, {{ Auth::user()->fullname }}</h4>
                        </li>
                        <!--<li class="hidden-xs hidden-sm">
                            <form class="navbar-search navbar-search-collapsed">
                            <div class="navbar-search-group">
                                <input class="navbar-search-input" type="text" placeholder="Search for people, companies, and more&hellip;">
                                <button class="navbar-search-toggler" title="Expand search form ( S )" aria-expanded="false" type="submit">
                                <span class="icon icon-search icon-lg"></span>
                                </button>
                                <button class="navbar-search-adv-btn" type="button">Advanced</button>
                            </div>
                            </form>
                        </li>-->
                        <li class="dropdown hidden-xs">
                            <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                                <img class="rounded" width="36" height="36" src="{{ asset('img/default-avatar.png') }}" alt=""> {{ Auth::user()->fullname }}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        Sign out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        <li class="visible-xs-block">
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <span class="icon icon-power-off icon-lg icon-fw"></span>
                                Sign out
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </nav>
                </div>
            </div>
        </div>
        <div class="layout-main">
            <div class="layout-sidebar">
                <div class="layout-sidebar-backdrop"></div>
                <div class="layout-sidebar-body">
                    <div class="custom-scrollbar">
                        <nav id="sidenav" class="sidenav-collapse collapse">
                            <ul class="sidenav">
                                <li class="sidenav-item active">
                                    <a href="{{ url('admin/dashboard') }}">
                                        <span class="sidenav-icon fa fa-home"></span>
                                        <span class="sidenav-label">Dashboard</span>
                                    </a>
                                </li>
                                <li class="sidenav-item has-subnav">
                                    <a href="#" aria-haspopup="true">
                                        <span class="sidenav-icon fa fa-users"></span>
                                        <span class="sidenav-label">User</span>
                                    </a>
                                    <ul class="sidenav-subnav collapse">
                                        <li class="sidenav-subheading">Customer</li>
                                        <li><a href="{{ url('admin/user?type=C') }}">Customer</a></li>
                                        <li><a href="{{ url('admin/user?type=A') }}">Agent</a></li>
                                    </ul>
                                </li>
                                <li class="sidenav-item">
                                    <a href="{{ url('admin/order') }}">
                                        <span class="sidenav-icon fa fa-shopping-cart"></span>
                                        <span class="sidenav-label">Order</span>
                                    </a>
                                </li>
                                <li class="sidenav-item">
                                    <a href="{{ url('admin/shop') }}">
                                        <span class="sidenav-icon fa fa-cutlery"></span>
                                        <span class="sidenav-label">Shop</span>
                                    </a>
                                </li>
                                <li class="sidenav-item">
                                    <a href="{{ url('admin/area') }}">
                                        <span class="sidenav-icon fa fa-map-marker"></span>
                                        <span class="sidenav-label">Area</span>
                                    </a>
                                </li>
                                <li class="sidenav-item">
                                    <a href="{{ url('admin/shop-category') }}">
                                        <span class="sidenav-icon fa fa-th-list"></span>
                                        <span class="sidenav-label">Shop Category</span>
                                    </a>
                                </li>
                                <li class="sidenav-item">
                                    <a href="{{ url('admin/fee') }}">
                                        <span class="sidenav-icon fa fa-money"></span>
                                        <span class="sidenav-label">Fee</span>
                                    </a>
                                </li>
                                <li class="sidenav-item">
                                    <a href="{{ url('admin/banner') }}">
                                        <span class="sidenav-icon fa fa-picture-o"></span>
                                        <span class="sidenav-label">Banner</span>
                                    </a>
                                </li>
                                <li class="sidenav-item">
                                    <a href="{{ url('admin/variable') }}">
                                        <span class="sidenav-icon fa fa-code"></span>
                                        <span class="sidenav-label">Variable</span>
                                    </a>
                                </li>
                                <li class="sidenav-item">
                                    <a href="{{ url('admin/qrcode') }}">
                                        <span class="sidenav-icon fa fa-qrcode"></span>
                                        <span class="sidenav-label">QR Code</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="layout-content">
                <div class="layout-content-body">
                    <div class="title-bar">
                        <ol class="breadcrumb">
                            <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                            @yield('breadcrumb')
                        </ol>
                    </div>
                    @yield('content')
                </div>
            </div>
            <div class="layout-footer">
                <div class="layout-footer-body">
                    <small class="copyright">{{ date('Y') }} &copy; <a href="http://yaitu.id/">Yaitu</a></small>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/vendor.min.js') }}"></script>
        <script src="{{ asset('js/elephant.min.js') }}"></script>
        <script src="{{ asset('js/application.min.js') }}"></script>
        <script src="{{ asset('js/demo.js') }}"></script>
        <script src="{{ asset('js/laravel.js') }}"></script>
        @include('admin.shared.js')
        @stack('js')
        {!! Toastr::render() !!}
    </body>
</html>