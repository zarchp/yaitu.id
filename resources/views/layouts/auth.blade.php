<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        
        <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
        <link rel="icon" type="image/ico" href="favicon-32x32.ico" sizes="32x32">
        <link rel="icon" type="image/ico" href="favicon-16x16.ico" sizes="16x16">
        <link rel="manifest" href="manifest.json">
        <link rel="mask-icon" href="safari-pinned-tab.svg" color="#0288d1">
        <meta name="theme-color" content="#ffffff">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
        <link rel="stylesheet" href="{{ asset('css/vendor.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/elephant.min.css') }}">
        @yield('css')
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
    </head>
    <body>
        @yield('content')
        
        <script src="{{ asset('js/vendor.min.js') }}"></script>
        <script src="{{ asset('js/elephant.min.js') }}"></script>
        @yield('js')
        {!! Toastr::render() !!}
    </body>
</html>
