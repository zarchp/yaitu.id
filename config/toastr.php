<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 05.08.2015
 * Time: 22:23
 */
return ['options' => [
    "progressBar" => false,
    "positionClass" =>"toast-bottom-right",
    "preventDuplicates"=> true,
    "showDuration" => 300,
    "hideDuration" => 1000,
    "timeOut" => 3000,
    "extendedTimeOut" => 1000,
    "showEasing" => "swing",
    "hideEasing"=> "linear",
    "showMethod" => "fadeIn",
    "hideMethod" => "fadeOut",
    "closeButton" => true,
]];